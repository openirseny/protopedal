# Protopedal

Compatibility tool for sim racing pedals and force feedback steering wheels.<br>
Helps with wheel and pedal detection by creating virtual devices with extended capabilities.<br>
Facilitates merging devices, range adjustments, custom curves, button to axis and axis to button mappings.

### Download and Usage

Builds from my debian machine are availble in the [releases section](../../releases).<br>
An [AUR package](https://aur.archlinux.org/packages/protopedal-git) is maintained by [Lawstorant](https://gitlab.com/Lawstorant).<br>
Typical usage:

`protopedal --name MyPedals --product 0001 /dev/input/by-id/usb-Thrustmaster_Sim_Pedals-event-if00`

If this does not already improve compatibility these options might help:

* `--buttons 1`
* `--axes 6`

More examples can be found in the [wiki](../../wikis/home)

### Manual Build

Requires `g++` and `libc-6-dev`:

`make`

### Options
```
protopedal [--help] [--name <name>] [--vendor <vendor>] [--product <product>]
 [--axis <axis> [--source <axis>] [--source-button <button>] [--min <min> --max <max>] [--invert] [--curve]]*
 [--button <button> [--source <button>] [--source-axis <axis>] [--min <min> --max <max>] [--invert] [--curve]]*
 [--axes <number>] [--buttons <number>] [--no-auto-axes] [--no-auto-buttons]
 [--gain <percent>] [--autocenter <percent>] [--ffb-device <index>] [--ffb-log <file>]
 [--grab] [--report-limits] [--verbose]  <device>+

 -h, --help                   Print this text
 <device>                     Physical device to mimic

Basic device information:
 -n, --name <name>            Virtual device name
 -v, --vendor <hex>           Virtual device vendor
 -p, --product <hex>          Virtual device product

Button and axis customization:
 -b, --button <button>        Virtual device button to customize
 -a, --axis <axis>            Virtual device axis to customize
 -s, --source <button>/<axis> Physical device button or axis to read from
 --source-axis <axis>         Physical device axis to read from
 --source-button <button>     Physical device button to read from
 -m, --min <number>           Lower bound
 -M, --max <number>           Upper bound
 -i, --invert                 Invert output
 -c, --curve <file>           Axis curve CSV file
 --buttons <number>           Minimum number of buttons advertized by the virtual device
 --axes <number>              Minimum number of axes advertized by the virtual device
 --no-auto-buttons            Do not create mappings for non-specified buttons
 --no-auto-axes               Do not create mappings for non-specified axes

Force feedback settings:
 --gain <percent>             Fixed force feedback gain
 --autocenter <percent>       Fixed auto centering
 --ffb-device <index>         Index of the device to advertize FFB capabilites from. Defaults to 0.
 --ffb-log <file>             Log file for force feedback effects

Other options:
 --grab                       Access the physical device exclusively, other programs do not receive events
 --report-limits              Report minimum and maximum physical axis values that appeared during testing
 --verbose                    Enable verbose output
 --daemon                     Continue operation when device gets lost

Devices can be referenced in <axis> and <button> by prepending the device index leading to the syntax
<index>:<capability> when using multiple physical devices
Unspecified options are read from the device to mimic.
By default existing axes and buttons are mapped to their physical counterparts.
Supported axes include X, Y, Z, RX, RY, RZ, THROTTLE, RUDDER, WHEEL, GAS and BRAKE among others.
Supported buttons include A, B, C, X, Y, Z, L1, R1, L2, R2, SELECT, START, MODE, LEFT-STICK, RIGHT-STICK, WHEEL, GEAR-DOWN and GEAR-UP among others.
```

### Background

Starting with Proton 5.13 some devices without button capabilities were not detected as joysticks and therefore not accessible in games running in Proton or wine.
This issue persisted up to at least Proton 7.0.<br>
Similar issues emerged with steering wheels when implementation details in the steam runtime changed. As a consequence this tool was rewritten to also support buttons and force feedback effects.<br>
Furthermore certain games implement their own device filtering and, for example, do not take input from<br>
(a) pedals with less than four axes or <br>
(b) pedals which lack axes X and Y but rather report through axes RX, RY and RZ.<br>
Protopedal circumvents the filtering issues by allowing the virtual device to have a wide number of buttons and axes. It supports force feedback, button and axis remappings as well as other calibration features.<br>
Read more about the issue [here](https://github.com/ValveSoftware/Proton/issues/5126) and [here](https://github.com/ValveSoftware/steam-runtime/issues/561).
Depending on your situation it might be sufficient to circumvent the steam runtime as discussed [here](https://github.com/Matoking/protontricks/issues/216#issuecomment-1501092876)

### Work in Progress

Features for future releases:
- Force feedback detail logging

If you encounter any difficulties or have some suggestions please open a new issue in the [issues section](https://gitlab.com/openirseny/protopedal/-/issues).

### Related Software
- jstest and jstest-gtk
- evtest
- evdev-joystick
- [virtjs](https://github.com/sambazley/virtjs)
- [xboxdrv](https://xboxdrv.gitlab.io/)
- [oversteer](https://github.com/berarma/oversteer)
