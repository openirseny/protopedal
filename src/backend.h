#ifndef PROTO_BACKEND_H
#define PROTO_BACKEND_H

#include <linux/input.h>
#include <linux/uinput.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#include <vector>
#include <string>
#include <stdexcept>
#include <cstdint>
#include <cstring>
#include <iostream>

#include "util.h"

namespace proto
{
	class Backend
	{
		public:
		struct EventBitmap {
			uint8_t map[EV_CNT/8 + 1];
		};
		struct KeyBitmap {
			uint8_t map[KEY_CNT/8 + 1];
		};
		struct AbsoluteBitmap {
			uint8_t map[ABS_CNT/8 + 1];
		};
		struct FFBBitmap {
			uint8_t map[FF_CNT/8 + 1];
		};
		struct Device {
			int file;
			std::string node;
			uinput_setup setup;
			uinput_user_dev dev; // legacy fallback structures
			bool created;
			bool opened;
			input_event pendingEvents[16];
			int pendingCapacity;
			int pendingStart;
			int pendingNo;
            bool grabbed;
		};
	private:
		struct PendingEvents {
			input_event* buffer;
			int capacity;
			int start;
			int count;
		};
	public:
		static int openRoot();
		
		static Device beginDeviceSetup(int root);
		static Device openDevice(const std::string& path);
		static void destroyDevice(Device& device);
		
		static void activateKeyEvents(Device& device);
		static void activateKey(Device& device, int code);
		
		static void activateAbsoluteEvents(Device& device);
		static void activateAbsoluteAxis(Device& device, int code, input_absinfo info);
		static void activateFFBEvents(Device& device, int effectNo);
		static void activateFFBEffect(Backend::Device& device, int code);
		
		static void completeDeviceParams(Device& device);
		static void finishDeviceSetup(Device& device);
		static bool checkDeviceConnected(Device& device);
		static bool checkDeviceExists(const std::string& path);
        static void grabDevice(Device& device);
        static void ungrabDevice(Device& device);
		
		static bool waitForEvent(Device& device, int timeout);
		static uint16_t waitForEvent(Device** devices, int timeout);
		static int getPendingEvents(Device& device);
		static input_event readEvent(Device& device, bool pop);
		
		static uinput_ff_upload beginUploadFFBEffect(Device& device, input_event trigger);
		static void endUploadFFBEffect(Device& device, uinput_ff_upload& effect);
		static uinput_ff_erase beginEraseFFBEffect(Device& device, input_event trigger);
		static void endEraseFFBEffect(Device& device, uinput_ff_erase& effect);
		
		static void sendSyncEvent(Device& device, int report);
		static void sendKeyEvent(Device& device, int code, bool pressed);
		static void sendAbsoluteAxis(Device& device, int code, int value);
		static void sendFFBGainEvent(Device& device, int gain);
		static void sendFFBAutoCenterEvent(Device& device, int autoCenter);
			
		static void sendFFBPlaybackEvent(Device& device, int id, int times);
		static void sendFFBUploadEvent(Device& device, uinput_ff_upload& effect);
		static void sendFFBEraseEvent(Device& device, uinput_ff_erase& effect);
		
		static EventBitmap getEventBitmap(Device& device);
		static KeyBitmap getKeyBitmap(Device& device);
		static AbsoluteBitmap getAbsoluteBitmap(Device& device);
		static FFBBitmap getFFBBitmap(Device& device);
		static input_absinfo getAbsoluteInfo(Device& device, int code);

	public:
		template <typename T>
		static bool isBitSet(T* bitmap, int code);
		template <typename T>
		static void setMapBit(T* bitmap, int code);

	private:
		static std::string appendError(std::string message, int error);
	};

}

#endif // PROTO_BACKEND_H
