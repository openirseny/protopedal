#ifndef PROTO_UTIL_H
#define PROTO_UTIL_H

#include <vector>
#include <string>
#include <algorithm>
#include <linux/input.h>

#ifndef ABS_MIN
#define ABS_MIN ABS_X
#endif 

#ifndef ABS_MAX
#define ABS_MAX ABS_MT_TOOL_Y
#endif

#ifndef BTN_MIN
#define BTN_MIN BTN_JOYSTICK
#endif

#ifndef BTN_MAX
#define BTN_MAX KEY_MAX
#endif

#ifndef BTN_CNT
#define BTN_CNT KEY_CNT
#endif

#ifndef ABS_NONE
#define ABS_NONE -1
#endif

#ifndef ABS_INVALID
#define ABS_INVALID (-0xFF)
#endif

#ifndef BTN_NONE
#define BTN_NONE -1
#endif

#ifndef BTN_INVALID
#define BTN_INVALID (-0xFF)
#endif

#ifndef EV_IDLE
#define EV_IDLE (EV_MAX + 1)
#endif

namespace proto
{

	class Util
	{
	private:
		struct NamedCapability {
			int code;
			std::string name;
		};
		static const NamedCapability buttonNames[];
		static const NamedCapability axisNames[];
	public:
		static bool extractDeviceFromCapability(const std::string& capability, int& device, std::string& name);
		static bool getAxisFromName(const std::string& name, int& axis);
		static bool getButtonFromName(const std::string& name, int& button);
		static std::string getAxisName(int axis);
		static std::string getDeviceAxisName(int device, int axis);
		static std::string getButtonName(int button);
		static std::string getDeviceButtonName(int device, int button);
		static std::string getEffectTypeName(int type);
	};

}

#endif // PROTO_UTIL_H
