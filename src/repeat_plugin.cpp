#include <linux/input.h>

#include <stdexcept>
#include <string>

#include "idle_event.h"
#include "button_event.h"
#include "virtual_device.h"
#include "device_setup.h"
#include "util.h"

namespace proto {
	extern "C" {
		struct ButtonConfig {
			int code;
			bool pressed;
			bool repeat;
			long since;
			long delay;
		};
		std::vector<ButtonConfig> configs;
		
		int buttons[4] = { BTN_TRIGGER_HAPPY5, BTN_TRIGGER_HAPPY11, BTN_TRIGGER_HAPPY19, BTN_TRIGGER_HAPPY20 };
		bool pressed[4] = { false, false, false, false };
		bool repeat[4] = { false, false, false, false };
		long since[4] = { 0, 0, 0, 0 };
		long span[4] = { 40, 40, 40, 40 };
		
		void init(DeviceSetup& setup, DeviceSetup::EventMap& mapping, int argc, char** args) {
			if (mapping.type != EV_KEY) {
				throw std::runtime_error("Repeat plugin can only be used with buttons");
			}
			ButtonConfig config = {};
			config.code = mapping.code;
			config.delay = 40;
			int button = mapping.code;
			int b = 0;
			for (; b < 4; b++) {
				if (buttons[b] == 0) {
					buttons[b] = button;
					break;
				}
			}
			if (argc > 2) {
				throw std::runtime_error("Repeat plugin takes only the delay in milliseconds as argument");
			}
			if (argc == 2) {
				int delay = std::atoi(args[1]);
				if (delay == 0) {
					throw std::runtime_error("Repeat plugin cannot read delay from argument");
				}
				config.delay = delay;
			}
			configs.push_back(config);
		}

		int map(DeviceSetup& setup, DeviceSetup::EventMap& mapping, DeviceEvent* source, DeviceEvent* target, int call) {
			if (source->getType() == EV_IDLE) {
				IdleEvent* sourceEvent = dynamic_cast<IdleEvent*>(source);
				if (sourceEvent != nullptr) {
					for (ButtonConfig& c : configs) {
						if (c.pressed && c.since > 0) {
							long elapsed = Logging::milliSecondsSinceInit() - c.since;
							if (elapsed > c.delay) {
								c.since = Logging::milliSecondsSinceInit();
								c.repeat ^= true;
								new (target) ButtonEvent(source->getDevice(), c.code, c.repeat);
								return 1;
							}
						}
					}
				}
			}
			if (source->getType() == EV_KEY) {
				ButtonEvent* sourceEvent = dynamic_cast<ButtonEvent*>(source);
				if (sourceEvent != nullptr) {
					int code = sourceEvent->getButton();
					bool press = sourceEvent->isPressed();
					for (ButtonConfig& c : configs) {
						if (code == c.code) {
							if (press != c.pressed) {
								c.pressed = press;
								c.repeat = press;
								c.since = press ? Logging::milliSecondsSinceInit() : 0;
							}
						}
					}
				}
			}
			return 0;
		}

		void quit(DeviceSetup& setup, DeviceSetup::EventMap& mapping) {
			// nothing to do
		}

	}
}

