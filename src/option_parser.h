#ifndef PROTO_OPTION_PARSER_H
#define PROTO_OPTION_PARSER_H

#include <getopt.h>

#include "util.h"
#include "device_mapper.h"
#include "device_setup.h"
#include "common_options.h"


namespace proto
{
	class DeviceSetup;

	class OptionParser
	{
	private:
		enum ErrorCode {
			OK = 0,
			HELP,
			INTERNAL_ERROR,
			INVALID_INPUT,
			UNRECOGNIZED_INPUT,
			SUPERFLUOUS_INPUT,
			DEVICE_NOT_SPECIFIED,
			EXPECTED_NUMBER,
			EXPECTED_POSITIVE_NUMBER,
			EXPECTED_VENDOR_PRODUCT_ID,
			CAPABILITY_NOT_SPECIFIED,
			CAPABILITY_ILLEGAL_FORMAT,
			AXIS_NOT_SUPPORTED,
			BUTTON_NOT_SUPPORTED,
			AXIS_PARAM_NOT_SUPPORTED,
			BUTTON_PARAM_NOT_SUPPORTED,
			CURVE_INVALID,
			PLUGIN_INVALID
		};
	public:
		OptionParser() {};
		~OptionParser() {};
	private:
		
		void displayUsage();
		void displayUsageError(const std::string& program, const std::string& param, const std::string& argument, const std::string& error);
		void displayButtons();
		void displayAxes();
	private:
		void clearMapping(DeviceSetup::EventMap& mapping);
		void beginAxisMapping(DeviceSetup::EventMap& mapping, int axis);
		void beginButtonMapping(DeviceSetup::EventMap& mapping, int button);
		void finalizeMapping(DeviceSetup& setup, DeviceSetup::EventMap& mapping, int& device, int& axis, int& button);
	public:
		bool parse(int argc, char** args, DeviceSetup& setup, CommonOptions& options);
	};

}

#endif // PROTO_OPTION_PARSER_H
