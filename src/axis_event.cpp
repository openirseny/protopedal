#include "axis_event.h"

namespace proto {
	AxisEvent::AxisEvent(int device, int axis, input_absinfo info) : DeviceEvent(),
		device(device),
		axis(axis),
		info(info) {
	}

	AxisEvent::~AxisEvent() {
	}

	int AxisEvent::getAxis() {
		return axis;
	}

	int AxisEvent::getType() {
		return EV_ABS;
	}
	
	int AxisEvent::getDevice() {
		return device;
	}
	
	int AxisEvent::getMinimum() {
		return info.minimum;
	}
	
	int AxisEvent::getMaximum() {
		return info.maximum;
	}
	
	int AxisEvent::getRange() {
		return std::max(1, info.maximum - info.minimum);
	}
	
	int AxisEvent::getValue() {
		return info.value;
	}

}
