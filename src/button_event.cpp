#include "button_event.h"

namespace proto {
	ButtonEvent::ButtonEvent(int device, int button, bool pressed) : DeviceEvent(),
		device(device),
		button(button),
		pressed(pressed) {
	}

	ButtonEvent::~ButtonEvent()
	{
	}

	int ButtonEvent::getType() {
		return EV_KEY;
	}
	
	int ButtonEvent::getDevice() {
		return device;
	}
	
	int ButtonEvent::getButton() {
		return button;
	}

	bool ButtonEvent::isPressed() {
		return pressed;
	}
}
