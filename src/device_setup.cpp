
#include "device_setup.h"

namespace proto {
	DeviceSetup::DeviceSetup() :
		vendor(0),
		product(0), 
		name(),
		devicePaths(),
		deviceRequired(),
		capabilitySetup(),
		capabilityMap(),
		minAxisNo(0),
		minButtonNo(0),
		maxAxisNo(ABS_CNT),
		maxButtonNo(BTN_CNT),
		fixedGain(-1),
		fixedAutoCenter(-1),
		reportAxisLimits(false),
		reportButtonLimits(false),
		exclusiveAccess(false),
		ffbDevice(0) {}
	
	void DeviceSetup::completeRanges(input_absinfo& sourceInfo, EventMap& mapInfo, AxisSetup& targetSetup, AxisSetup& fallback) {
		// original
		if (mapInfo.minimum == INT_MIN) {
			mapInfo.minimum = sourceInfo.minimum;
		}
		if (mapInfo.maximum == INT_MIN) {
			mapInfo.maximum = sourceInfo.maximum;
		}
		if (targetSetup.minimum == INT_MIN) {
			targetSetup.minimum = mapInfo.minimum;
		}
		if (targetSetup.maximum == INT_MIN) {
			targetSetup.maximum = mapInfo.maximum;
		}
		if (targetSetup.minimum == INT_MIN) {
			targetSetup.minimum = fallback.minimum;
		}
		if (targetSetup.maximum == INT_MIN) {
			targetSetup.maximum = fallback.maximum;
		}
		if (mapInfo.minimum == INT_MIN) {
			mapInfo.minimum = targetSetup.minimum;
		}
		if (mapInfo.maximum == INT_MIN) {
			mapInfo.maximum = targetSetup.maximum;
		}
		if (mapInfo.minimum > mapInfo.maximum) {
			int minimum = mapInfo.minimum;
			mapInfo.minimum = mapInfo.maximum;
			mapInfo.maximum = minimum;
			mapInfo.invert = true;
		}
		if (targetSetup.minimum > targetSetup.maximum) {
			int minimum = targetSetup.minimum;
			targetSetup.minimum = targetSetup.maximum;
			targetSetup.maximum = minimum;
		}
	}
	
	int DeviceSetup::mapToRange(int value, int inmin, int inmax, int outmin, int outmax, bool invert) {
		int offset0 = value - inmin;
		int range0 = inmax - inmin;
		int range1 = outmax - outmin;
		if (range0 <= 0 || range1 <= 0) {
			return outmin;
		}
		int offset1 = offset0;
		if (range1 != range0) {
			offset1 = ((int64_t)offset0*range1)/range0;
		}
		int out1 = invert ? outmax - offset1 : outmin + offset1;
		if (out1 < outmin) {
			out1 = outmin;
		}
		if (out1 > outmax) {
			out1 = outmax;
		}
		return out1;
	}
	
	DeviceSetup::AxisSetup DeviceSetup::toAxisSetup(input_absinfo& info) {
		return { info.minimum, info.maximum };
	}
	
	bool DeviceSetup::addDevicePath(DeviceSetup& setup, const std::string& path, bool required) {
		if (setup.devicePaths.size() >= sizeof(uint16_t)*8) {
			return false;
		}
		setup.devicePaths.push_back(path);
		setup.deviceRequired.push_back(required);
		return true;
	}

	void DeviceSetup::setName(DeviceSetup& setup, const std::string& name) {
		setup.name = name;
	}

	void DeviceSetup::setVendor(DeviceSetup& setup, int vendor) {
		setup.vendor = vendor;
	}

	void DeviceSetup::setProduct(DeviceSetup& setup, int product) {
		setup.product = product;
	}
	
	DeviceSetup::CapabilityKey DeviceSetup::toCapability(EventSource& source) {
		CapabilityKey result = { source.type, source.code };
		return result;
	}
	
	DeviceSetup::CapabilityKey DeviceSetup::toCapability(EventMap& map) {
		CapabilityKey result = { map.type, map.code };
		return result;
	}
	
	void DeviceSetup::mapCapability(DeviceSetup& setup, EventSource& source, EventMap& mapInfo) {
		CapabilityKey key = DeviceSetup::toCapability(mapInfo);
		enableCapability(setup, key);
		setup.capabilityMap.insert({ source, mapInfo });
	}
	
	void DeviceSetup::enableCapability(DeviceSetup& setup, CapabilityKey& key) {
		auto iter = setup.capabilitySetup.find(key);
		if (iter == setup.capabilitySetup.end()) {
			if (key.type == EV_ABS) {
				CapabilitySetup props;
				props.axis = { INT_MIN, INT_MIN };
				setup.capabilitySetup[key] = props;
			} else if (key.type == EV_KEY) {
				CapabilitySetup props;
				props.button = { false };
				setup.capabilitySetup[key] = props;
			}
		}
	}
	
	void DeviceSetup::setMinAxisNo(DeviceSetup& setup, unsigned int axes) {
		setup.minAxisNo = axes;
	}
	
	void DeviceSetup::setMinButtonNo(DeviceSetup& setup, unsigned int buttons) {
		setup.minButtonNo = buttons;
	}
	
	
	void DeviceSetup::setMaxAxisNo(DeviceSetup& setup, unsigned int axes) {
		setup.maxAxisNo = axes;
	}
	
	void DeviceSetup::setMaxButtonNo(DeviceSetup& setup, unsigned int buttons) {
		setup.maxButtonNo = buttons;
	}
	
	void DeviceSetup::setFixedGain(DeviceSetup& setup, int gain) {
		setup.fixedGain = gain;
	}
	
	void DeviceSetup::setFixedAutoCenter(DeviceSetup& setup, int autoCenter) {
		setup.fixedAutoCenter = autoCenter;
	}
	
	void DeviceSetup::reportCapabilityLimits(DeviceSetup& setup) {
		setup.reportAxisLimits = true;
		setup.reportButtonLimits = true;
	}
	
	void DeviceSetup::enableExclusiveAccess(DeviceSetup& setup) {
        setup.exclusiveAccess = true;
    }
	
	void DeviceSetup::setFFBDevice(DeviceSetup& setup, int device) {
		setup.ffbDevice = device;
	}
}
