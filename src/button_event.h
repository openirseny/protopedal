#ifndef PROTO_BUTTON_EVENT_H
#define PROTO_BUTTON_EVENT_H

#include "device_event.h"
#include <linux/input.h>

namespace proto
{
	class ButtonEvent : public DeviceEvent
	{
	private:
		int device;
		int button;
		bool pressed;
	public:
		ButtonEvent(int device, int button, bool pressed);
		~ButtonEvent();
	public:
		int getType() override;
		int getDevice() override;
		int getButton();
		bool isPressed();
	};

}

#endif // PROTO_Button_EVENT_H
