#ifndef CURVEDESCRIPTION_H
#define CURVEDESCRIPTION_H

#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <fstream>

namespace proto
{

	class CurveDescription {
	private:
		struct Support {
			float input;
			float output;
		};
	private:
		std::vector<Support> points;
	public:
		CurveDescription();
		~CurveDescription();
	public:
		void addSupport(float input, float output);
		void normalize();
		int apply(int input, int inmin, int inmax, int outmin, int outmax, bool invert);
		void clear();
		void loadCSV(const std::string& file);
	};

}

#endif
