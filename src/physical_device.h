#ifndef PROTO_PHYSICAL_DEVICE_H
#define PROTO_PHYSICAL_DEVICE_H

#include <string>
#include <queue>
#include <stdexcept>
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <climits>
#include "backend.h"
#include "device_event.h"
#include "logging.h"
#include "util.h"

namespace proto
{

	class PhysicalDevice
	{
	private:
		struct AxisInfo {
			int calMinimum;
			int calMaximum;
			int seenMinimum;
			int seenMaximum;
			int fuzz;
			int flat;
			int resolution;
		};
		struct ButtonInfo {
			int seenMinimum;
			int seenMaximum;
		};
		struct EffectInfo {
			int id;
			int playback;
		};
	public:
		static bool checkExists(const std::string& path);
		static int collectEvents(std::vector<PhysicalDevice>& phys);
	private:
		int index;
		Backend::Device device;
		std::string path;
		int setupState;
		Backend::EventBitmap supportedEvents;
		Backend::AbsoluteBitmap supportedAxes;
		Backend::KeyBitmap supportedButtons;
		Backend::FFBBitmap supportedEffects;
		std::unordered_map<int, AxisInfo> axisInfo;
		std::unordered_map<int, ButtonInfo> buttonInfo;
		std::unordered_map<int, EffectInfo> effectMap;
	public:
		PhysicalDevice(int index);
		~PhysicalDevice();
	public:
		int getIndex();
		int collectEvents();
		DeviceEvent* nextEvent();
		bool feedbackEvent(DeviceEvent* event);
		bool sendEvent(DeviceEvent* event);
		bool isOpen();
		bool open(const std::string& path);
		bool readSetup();
		void close();
		std::string getName();
		std::string getPath();
		unsigned short getVendor();
		unsigned short getProduct();
		int getFFBEffectNo();
        bool setExclusiveAccess(bool exclusive) ;
		bool hasEvent(int event);
		bool hasButton(int button);
		bool getAxisInfo(int axis, input_absinfo& info);
		bool getAxisLimits(int axis, int& minimum, int& maximum);
		bool getButtonLimits(int button, int& minimum, int& maximum);
		bool hasFFBEffect(int effect);
		bool checkConnected();
	private:
		DeviceEvent* makeEvent(input_event& event);
	};

}

#endif // PROTO_PHYSICAL_DEVICE_H
