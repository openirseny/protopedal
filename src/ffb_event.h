#ifndef PROTO_FFB_UPLOAD_EVENT_H
#define PROTO_FFB_UPLOAD_EVENT_H

#include <linux/uinput.h>
#include <cstdint>
#include <algorithm>
#include <cstring>
#include "device_event.h"

namespace proto
{

	class FFBEvent : public DeviceEvent
	{
	public:
		enum class PayloadType:int {
			GAIN,
			AUTOCENTER,
			UPLOAD,
			ERASE,
			PLAYBACK,
		};
		
	private:
		struct FFBPayload {
			uint8_t payload[std::max(sizeof(uinput_ff_upload), sizeof(uinput_ff_erase))];
		};
		PayloadType type;
		FFBPayload payload;
	private:
		FFBEvent(PayloadType type);
	public:
		FFBEvent(uinput_ff_upload& upload);
		FFBEvent(uinput_ff_erase& erase);
		FFBEvent(int value, bool gain);
		FFBEvent(int id, int timesToPlay);
		~FFBEvent();
	public:
		int getType() override;
		int getDevice() override;
		int getID();
		void setID(int id);
		void setResult(int errorCode);
		PayloadType getPayloadType();
		int* getGainPayload();
		int* getAutoCenterPayload();
		uinput_ff_upload* getUploadPayload();
		uinput_ff_erase* getErasePayload();
		int* getPlaybackPayload();
	};

}

#endif // PROTO_FFB_UPLOAD_EVENT_H
