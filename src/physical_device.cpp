#include "physical_device.h"
#include "axis_event.h"
#include "button_event.h"
#include "ffb_event.h"
#include "sync_event.h"

namespace proto {

	PhysicalDevice::PhysicalDevice(int index) :
		index(index),
		device(),
		path(),
		setupState(0),
		supportedEvents(),
		supportedAxes(),
		supportedButtons(),
		supportedEffects(),
		axisInfo(),
		buttonInfo(),
		effectMap() {
	}

	PhysicalDevice::~PhysicalDevice() {
		close();
	}

	bool PhysicalDevice::isOpen() {
		return setupState == 2;
	}
	
	int PhysicalDevice::getIndex() {
		return index;
	}
	
	bool PhysicalDevice::open(const std::string& path) {
		if (setupState != 0) {
			return false;
		}
		try {
			device = Backend::openDevice(path);
		} catch (std::exception& e) {
			Logging::beginError(8) << "Failed to open " << path << ": " << e.what();
			Logging::endError(8);
			return false;
		}
		setupState = 1;
		this->path = path;
		try {
			// reading the setup (below) may fail and the program exists
			// read the event types early to print out interesting info
			// failing to read them is not critical
			supportedEvents = Backend::getEventBitmap(device);
		} catch (std::exception &e) {
			Logging::beginError(1) << "Event bitmap not readable: " << e.what();
			Logging::endError(1);
		}
		return true;
	}
	

	bool PhysicalDevice::readSetup() {
		if (setupState != 1) {
			return false;
		}
		try {
			Backend::completeDeviceParams(device);
			supportedAxes = Backend::getAbsoluteBitmap(device);
			supportedButtons = Backend::getKeyBitmap(device);
			supportedEffects = Backend::getFFBBitmap(device);
		} catch (std::exception& e) {
			Logging::beginError(8) << "Failed to read setup: " << e.what();
			Logging::endError(8);
			return false;
		}
		for (int a = ABS_MIN; a <= ABS_MAX; a++) {
			if (Backend::isBitSet(&supportedAxes, a)) {
				input_absinfo absInfo = Backend::getAbsoluteInfo(device, a);
				AxisInfo info = {
					absInfo.minimum,
					absInfo.maximum,
					absInfo.maximum,
					absInfo.minimum,
					absInfo.fuzz,
					absInfo.flat,
					absInfo.resolution
				};
				axisInfo[a] = info;
			}
		}
		for (int b = BTN_MIN; b <= BTN_MAX; b++) {
			if (Backend::isBitSet(&supportedButtons, b)) {
				ButtonInfo info = {
					1, 0
				};
				buttonInfo[b] = info;
			}
		}
		setupState = 2;
		return true;
	}
	
	bool PhysicalDevice::checkConnected() {
		if (setupState < 1) {
			return false;
		}
		return Backend::checkDeviceConnected(device);
	}
	
	bool PhysicalDevice::checkExists(const std::string& path) {
		return Backend::checkDeviceExists(path);
	}

	std::string PhysicalDevice::getName() {
		if (setupState != 2) {
			return "";
		}
		return std::string(device.setup.name);
	}

	std::string PhysicalDevice::getPath() {
		if (setupState != 2) {
			return "";
		}
		return device.node;
	}

	unsigned short PhysicalDevice::getVendor() {
		if (setupState != 2) {
			return 0;
		}
		return device.setup.id.vendor;
	}

	unsigned short PhysicalDevice::getProduct() {
		if (setupState != 2) {
			return 0;
		}
		return device.setup.id.product;
	}

	int PhysicalDevice::getFFBEffectNo() {
		if (setupState != 2) {
			return 0;
		}
		return device.setup.ff_effects_max;
	}
	
	
	bool PhysicalDevice::setExclusiveAccess(bool exclusive) {
        if (setupState != 2) {
			return false;
		}
        try {            
            if (exclusive) {
                Backend::grabDevice(device);
            } else {
                Backend::ungrabDevice(device);
            }
        } catch (std::exception& e) {
            return false;
        }
        return true;
    }

	void PhysicalDevice::close() {
		if (setupState < 1) {
			return;
		}
		try {
			Backend::destroyDevice(device);
		} catch (std::exception& e) {
			Logging::beginError(2) << e.what();
			Logging::endError(2);
		}
		for (auto it = effectMap.begin(); it != effectMap.end(); it++) {
			EffectInfo& info = it->second;
			info.id = -1;
		}
		setupState = 0;
	}

	bool PhysicalDevice::getAxisInfo(int axis, input_absinfo& info) {
		if (setupState < 1) {
			return false;
		}
		auto iter = axisInfo.find(axis);
		if (iter == axisInfo.end()) {
			return false;
		}
		info.minimum = iter->second.calMinimum;
		info.maximum = iter->second.calMaximum;
		info.fuzz = iter->second.fuzz;
		info.flat = iter->second.flat;
		info.resolution = iter->second.resolution;
		return true;
	}
	
	bool PhysicalDevice::getAxisLimits(int axis, int& minimum, int& maximum) {
		if (setupState < 1) {
			return false;
		}
		auto iter = axisInfo.find(axis);
		if (iter == axisInfo.end()) {
			return false;
		}
		if (iter->second.seenMaximum < iter->second.seenMinimum) {
			return false;
		}
		minimum = iter->second.seenMinimum;
		maximum = iter->second.seenMaximum;
		return true;
	}
	
	bool PhysicalDevice::getButtonLimits(int button, int& minimum, int& maximum) {
		if (setupState < 1) {
			return false;
		}
		auto iter = buttonInfo.find(button);
		if (iter == buttonInfo.end()) {
			return false;
		}
		if (iter->second.seenMaximum < iter->second.seenMinimum) {
			return false;
		}
		minimum = iter->second.seenMinimum;
		maximum = iter->second.seenMaximum;
		return true;
	}
	
	bool PhysicalDevice::hasEvent(int event) {
		if (setupState < 1) {
			return false;
		}
		return Backend::isBitSet(&supportedEvents, event);
	}

	bool PhysicalDevice::hasButton(int button) {
		if (setupState < 1) {
			return false;
		}
		return Backend::isBitSet(&supportedButtons, button);
	}

	bool PhysicalDevice::hasFFBEffect(int effect) {
		if (setupState < 1) {
			return false;
		}
		return Backend::isBitSet(&supportedEffects, effect);
	}
	
	int PhysicalDevice::collectEvents(std::vector<PhysicalDevice>& phys) {
		size_t size = sizeof(uint16_t)*8;
		Backend::Device* devices[size];
		memset(devices, 0, sizeof(Backend::Device*)*size);
		for (size_t i = 0, d = 0; i < phys.size() && d < size; i++) {
			if (phys[i].setupState >= 1) {
				devices[d] = &phys[i].device;
				d += 1;
			}
		}
		uint16_t map = 0;
		try {
			map = Backend::waitForEvent(devices, 1);
		} catch (std::exception& e) {
			return 0;
		}
		if (map == 0) {
			return 0;
		}
		int result = 0;
		for (size_t i = 0, d = 0; i < phys.size() && d < size; i++) {
			if (phys[i].setupState >= 1) {
				uint16_t mask = 0x1<<d;
				if ((map&mask) != 0) {
					try {
						Backend::readEvent(phys[i].device, false);
						result += 1;
					} catch (std::exception& e) {
						// ignore
					}
				}
				d += 1;
			}
		}
		return result;
	}

	int PhysicalDevice::collectEvents() {
		if (setupState < 1) {
			return 0;
		}
		try {
			if (Backend::waitForEvent(device, 1)) {
				Backend::readEvent(device, false);
			}
		} catch (std::exception& e) {
			return 0;
		}
		return Backend::getPendingEvents(device);
	}

	DeviceEvent* PhysicalDevice::nextEvent() {
		if (setupState < 1) {
			return 0;
		}
		if (Backend::getPendingEvents(device) == 0) {
			return nullptr;
		}
		try {
			input_event source = Backend::readEvent(device, true);
			return makeEvent(source);
		} catch (std::exception& e) {
			return nullptr;
		}
	}

	bool PhysicalDevice::feedbackEvent(DeviceEvent* event) {
		delete event;
		return true;
	}

	bool PhysicalDevice::sendEvent(DeviceEvent* event) {
		bool result = true;
		if (event->getType() == EV_FF) {
			if (FFBEvent* ffb = dynamic_cast<FFBEvent*>(event)) {
				int sourceID = ffb->getID();
				EffectInfo newInfo = { -1, 0 };
				EffectInfo& targetInfo = newInfo;
				if (sourceID > -1) 
				{
					auto iter = effectMap.find(sourceID);
					if (iter != effectMap.end()) {
						targetInfo = iter->second;
					}
				}
				switch (ffb->getPayloadType()) {
					case FFBEvent::PayloadType::GAIN: 
						if (setupState == 2) {
							int gain = *ffb->getGainPayload();
							Logging::beginFile(4) << "Setting gain to " << gain;
							try {
								Backend::sendFFBGainEvent(device, gain);
							} catch (std::exception& e) {
								Logging::appendFile(4, "failed: ") << e.what();
								result = false;
							}
							Logging::endFile(4);
						}
						break;
					case FFBEvent::PayloadType::AUTOCENTER: {
						if (setupState == 2) {
							int autoCenter = *ffb->getAutoCenterPayload();
							Logging::beginFile(4) << "Setting autocenter to " << autoCenter;
							try {
								Backend::sendFFBAutoCenterEvent(device, autoCenter);
							} catch (std::exception& e) {
								Logging::appendFile(4, " failed: ") << e.what();
								result = false;
							}
							Logging::endFile(4);
						}
						break;
					}
					case FFBEvent::PayloadType::PLAYBACK: {
						int count = *ffb->getPlaybackPayload();
						if (targetInfo.id > -1) {
							Logging::beginFile(4);
							if (count > 0) {
								Logging::appendFile(4, "Playing effect ") << targetInfo.id << " (" << count << " times)";
							} else {
								Logging::appendFile(4, "Stopping effect ") << targetInfo.id;
							}
							try {
								if (setupState == 2) {
									Backend::sendFFBPlaybackEvent(device, targetInfo.id, count);
								}
							} catch (std::exception& e) {
								Logging::appendFile(4, " failed: ") << e.what();
								result = false;
							}
							Logging::endFile(4);
						} 
						// keep track of playback
						targetInfo.playback = count;
						effectMap[sourceID] = targetInfo;
						break;
					}
					case FFBEvent::PayloadType::UPLOAD: {
						uinput_ff_upload upload = *ffb->getUploadPayload();
						// exchange the source with the target effect id for the upload
						// do not touch the source id in the provided event
						upload.effect.id = targetInfo.id;
						if (upload.old.id > -1) {
							upload.old.id = targetInfo.id;
						}
						bool play = false;
						std::ostream& log = Logging::beginFile(4);
						if (targetInfo.id < 0) {
							play = targetInfo.playback > 0;
							log << "Uploading new effect";
						} else {
							log << "Uploading effect " << targetInfo.id;
						}
						log << " (" << Util::getEffectTypeName(upload.effect.type) << ")";
						try {
							if (setupState == 2) {
								Backend::sendFFBUploadEvent(device, upload);
							}
							// if the target effect id was less than zero
							// the kernel/driver should have assigned a new id
							if (targetInfo.id < 0) {
								targetInfo.id = upload.effect.id;
								log << " as " << upload.effect.id;
							}
						} catch (std::exception& e) {
							ffb->setResult(1);
							log << " failed: " << e.what();
							play = false;
						}
						Logging::endFile(4);
						if (play && setupState == 2) {
							// if the physical device was disconnected we should restore the playback state
							// as applications talking to the virtual device may not restart effects
							// that are already considered running
							Logging::beginFile(4);
							log << "Automatically playing effect " << targetInfo.id;
							try {
								Backend::sendFFBPlaybackEvent(device, targetInfo.id, targetInfo.playback);
							} catch (std::exception& e) {
								log << " failed: " << e.what();
								result = false;
							}
							Logging::endFile(4);
						}
						if (sourceID < 0) {
							// the effect id sent to the virtual device should already be assigned by the kernel
							// in case it is not we simply assign the same id we got from the physical device
							sourceID = targetInfo.id;
							ffb->setID(targetInfo.id);
						}
						effectMap[sourceID] = targetInfo;
						break;
					}
					case FFBEvent::PayloadType::ERASE:
						if (targetInfo.id > -1) {
							Logging::beginFile(4) << "Erasing effect " << targetInfo.id;
							uinput_ff_erase erase = *ffb->getErasePayload();
							erase.effect_id = targetInfo.id;
							try {
								if (setupState == 2) {
									Backend::sendFFBEraseEvent(device, erase);
								}
								effectMap.erase(sourceID);
								targetInfo = newInfo;
							} catch (std::exception& e) {
								ffb->setResult(1);
								Logging::appendFile(4, " failed: ") << e.what();
								result = false;
							}
							Logging::endFile(4);
							targetInfo.playback = 0;
						}
						break;
				}
			}
		}
		return result;
	}

	DeviceEvent* PhysicalDevice::makeEvent(struct input_event& event) {
		switch (event.type) {
			case EV_SYN:
				if (event.code == SYN_DROPPED) {
					Logging::beginError(5);
					Logging::lineError(5, "Syn dropped (missed events)");
				}
				return new SyncEvent(event.code);
			case EV_ABS: {
				struct input_absinfo info = {};
				auto pair = axisInfo.find(event.code);
				if (pair != axisInfo.end()) {
					info.minimum = pair->second.calMinimum;
					info.maximum = pair->second.calMaximum;
					info.fuzz = pair->second.fuzz;
					info.flat = pair->second.flat;
					info.resolution = pair->second.resolution;
					if (event.value < pair->second.seenMinimum) {
						pair->second.seenMinimum = event.value;
					}
					if (event.value > pair->second.seenMaximum) {
						pair->second.seenMaximum = event.value;
					}
				}
				info.value = event.value;
				return new AxisEvent(index, event.code, info);
			}
			case EV_KEY: {
				auto pair = buttonInfo.find(event.code);
				if (pair != buttonInfo.end()) {
					if (event.value < pair->second.seenMinimum) {
						pair->second.seenMinimum = event.value;
					}
					if (event.value > pair->second.seenMaximum) {
						pair->second.seenMaximum = event.value;
					}
				}
				return new ButtonEvent(index, event.code, event.value);
			}
			default:
				return nullptr;
		}
	}
}

