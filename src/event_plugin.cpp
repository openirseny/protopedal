#include "event_plugin.h"

namespace proto {
	EventPlugin::EventPlugin() : 
		lib(nullptr),
		initSym(nullptr), 
		quitSym(nullptr),
		mapSym(nullptr) {
	}
	
	EventPlugin::~EventPlugin() {
		unload();
	}
	
	void EventPlugin::init(DeviceSetup& setup, DeviceSetup::EventMap& mapping) {
		if (initSym == nullptr) {
			return;
		}
		std::vector<std::string> split;
		split.push_back("plugin");
		splitArgs(mapping.args, split);
		std::vector<char*> input(split.size() + 1, nullptr);
		for (size_t i = 0; i < split.size(); i++) {
			input[i] = (char*)split[i].c_str();
		}
		auto init = (void (*)(DeviceSetup&, DeviceSetup::EventMap&, int, char**))initSym;
		init(setup, mapping, input.size() - 1, input.data());
	}
	
	void EventPlugin::quit(DeviceSetup& setup, DeviceSetup::EventMap& mapping) {
		if (quitSym == nullptr) {
			return;
		}
		auto quit = (void (*)(DeviceSetup&, DeviceSetup::EventMap&))quitSym;
		quit(setup, mapping);
	}
	
	int EventPlugin::map(DeviceSetup& setup, DeviceSetup::EventMap& mapping, DeviceEvent* source, DeviceEvent* target, int call) {
		if (mapSym == nullptr) {
			return 0;
		}
		auto map = (int (*)(DeviceSetup&, DeviceSetup::EventMap&, DeviceEvent*, DeviceEvent*, int))mapSym;
		return map(setup, mapping, source, target, call);
	}
	
	void EventPlugin::loadSO(const std::string& file) {
		unload();
		int errorCode = 0;
		do {
			dlerror(); // clear old error condition
			lib = dlopen(file.c_str(), RTLD_LAZY | RTLD_LOCAL);
			if (lib == nullptr) {
				errorCode = 1;
				break;
			}
			initSym = dlsym(lib, "init");
			if (initSym == nullptr) {
				errorCode = 2;
				break;
			}
			mapSym = dlsym(lib, "map");
			if (mapSym == nullptr) {
				errorCode = 3;
				break;
			}
			quitSym = dlsym(lib, "quit");
			if (quitSym == nullptr) {
				errorCode = 4;
				break;
			}
		} while (false);
		if (errorCode != 0) {
			std::string error = dlerror();
			unload();
			if (error.empty()) {
				switch (errorCode) {
				case 1:
					error = "Failed to open plugin file";
					break;
				case 2:
					error = "Failed to load init function from plugin";
					break;
				case 3:
					error = "Failed to load map function from plugin";
					break;
				case 4:
					error = "Failed to load quit function from plugin";
					break;
				}
			}
			throw std::runtime_error(error);
		}
	}
	
	void EventPlugin::unload() {
		if (lib != nullptr) {
			dlclose(lib);
			lib = nullptr;
		}
		initSym = quitSym = mapSym = nullptr;
	}
	
	void EventPlugin::splitArgs(const std::string& args, std::vector<std::string>& split) {
		int start = 0;
		int end = args.length() - 1;
		if (end <= 1) {
			return;
		}
		if (args[0] == '\'' || args[0] == '"') {
			start += 1;
			if (args[end] == args[0]) {
				end -= 1;
			}
		}
		for (int c = start; c <= end; c++) {
			if (args[c] == ' ' || args[c] == '\t') {
				if (c > start) {
					split.push_back(std::string(args, start, c - start));
				}
				start = c + 1;
			}
		}
		if (start <= end) {
			split.push_back(std::string(args, start, end - start + 1));
		}
	}
}