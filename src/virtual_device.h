#ifndef PROTO_VIRTUAL_DEVICE_H
#define PROTO_VIRTUAL_DEVICE_H

#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <stdexcept>
#include "backend.h"
#include "device_event.h"
#include "logging.h"

namespace proto {
	class VirtualDevice
	{
	private:
		Backend::Device device;
		std::unordered_map<int, bool> buttons;
		std::unordered_map<int, input_absinfo> axes;
		std::unordered_set<int> effects;
		std::unordered_map<int, uinput_ff_upload> upload;
		bool absEnabled;
		bool keyEnabled;
		int ffbEnabled;
		int setupState;
		
	public:
		VirtualDevice();
		~VirtualDevice();
	public:
		int collectEvents();
		DeviceEvent* nextEvent();
		bool feedbackEvent(DeviceEvent* event);
		bool sendEvent(DeviceEvent* event);
		bool beginOpen();
		bool finishOpen();
		void close();
		bool isOpen();
		bool enableAxes();
		bool addAxis(int axis, input_absinfo info);
		bool getAxisInfo(int axis, input_absinfo& info);
		bool enableButtons();
		bool hasButton(int button);
		bool addButton(int key);
		bool getButtonState(int button);
		bool enableFFB(int maximum);
		bool addFFBEffect(int effect);
		bool setName(const std::string& name);
		bool setVendor(int vendor);
		bool setProduct(int product);
	private:
		DeviceEvent* makeEvent(input_event& event);
		
	private:
		void checkSetupState(int expected);
	};
}
#endif // PROTO_VIRTUAL_DEVICE_H
