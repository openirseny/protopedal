#ifndef PROTO_DEVICE_MAPPER_H
#define PROTO_DEVICE_MAPPER_H

#include <linux/input.h>
#include <linux/uinput.h>
#include <climits>
#include <algorithm>
#include <stdexcept>
#include <sstream>
#include "logging.h"
#include "device_event.h"
#include "physical_device.h"
#include "virtual_device.h"
#include "device_setup.h"
#include "button_event.h"
#include "axis_event.h"
#include "ffb_event.h"
#include "sync_event.h"
#include "idle_event.h"
#include "curve_description.h"
#include "event_plugin.h"

namespace proto
{
	class AxisEvent;
	class ButtonEvent;
	class FFBEvent;
	
	class DeviceMapper
	{
	private:
		std::unordered_map<int, ff_effect> effectMap;
	public:
		DeviceMapper();
		~DeviceMapper();
	public:
		bool initialize(DeviceSetup& setup, std::vector<PhysicalDevice>& phys, VirtualDevice& virt);
		bool initializePhys(DeviceSetup& setup, PhysicalDevice& phys);
		void finishPhys(DeviceSetup& setup, PhysicalDevice& phys);
		bool continueMapping(DeviceSetup& setup, std::vector<PhysicalDevice>& phys, VirtualDevice& virt);
		void finish(DeviceSetup& setup, std::vector<PhysicalDevice>& phys, VirtualDevice& virt);
	private:
		bool initializeParams(DeviceSetup& setup, PhysicalDevice& phys, VirtualDevice& virt);
		bool initializeAxes(DeviceSetup& setup, std::vector<PhysicalDevice>& phys, VirtualDevice& virt);
		bool initializeButtons(DeviceSetup& setup, std::vector<PhysicalDevice>& phys, VirtualDevice& virt);
		bool initializeFFBEffects(DeviceSetup& setup, PhysicalDevice& phys, VirtualDevice& virt);
		bool beginFFBEffects(DeviceSetup& setup, PhysicalDevice& phys);
		bool mapAxisToAxisEvent(DeviceSetup& setup, AxisEvent& source, AxisEvent& target, DeviceSetup::EventMap& mapping, VirtualDevice& virt);
		bool mapAxisToButtonEvent(DeviceSetup& setup, AxisEvent& source, ButtonEvent& target, DeviceSetup::EventMap& mapping, VirtualDevice& virt);
		bool mapButtonToButtonEvent(DeviceSetup& setup, ButtonEvent& source, ButtonEvent& target, DeviceSetup::EventMap& mapping, VirtualDevice& virt);
		bool mapButtonToAxisEvent(DeviceSetup& setup, ButtonEvent& source, AxisEvent& target, DeviceSetup::EventMap& mapping, VirtualDevice& virt);
		bool mapFFBtoFFBEvent(DeviceSetup& setup, FFBEvent& source, FFBEvent& target, VirtualDevice& virt);
		bool mapEvent(DeviceSetup& setup, PhysicalDevice& phys, DeviceEvent* event, VirtualDevice& virt, bool enablePlugins);
		bool mapThroughPlugin(DeviceSetup& setup, PhysicalDevice& phys, DeviceSetup::EventMap& mapping, DeviceEvent* event, VirtualDevice& virt);
		void finishVirt(DeviceSetup& setup, VirtualDevice& virt);
	};

}

#endif // PROTO_DEVICE_MAPPER_H
