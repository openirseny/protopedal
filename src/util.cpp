#include "util.h"

namespace proto {
	
	const Util::NamedCapability Util::axisNames[] = { 
		{ ABS_NONE, "NONE" },
		{ ABS_X, "X" },
		{ ABS_Y, "Y" }, 
		{ ABS_Z, "Z" },
		{ ABS_RX, "RX" },
		{ ABS_RY, "RY" },
		{ ABS_RZ, "RZ" },
		{ ABS_THROTTLE, "THROTTLE" },
		{ ABS_RUDDER, "RUDDER" },
		{ ABS_WHEEL, "WHEEL" },
		{ ABS_GAS, "GAS" },
		{ ABS_BRAKE, "BRAKE" },
		{ ABS_HAT0X, "HAT0-X" },
		{ ABS_HAT0Y, "HAT0-Y" },
		{ ABS_HAT1X, "HAT1-X" },
		{ ABS_HAT1Y, "HAT1-Y" },
		{ ABS_HAT2X, "HAT2-X" },
		{ ABS_HAT2Y, "HAT2-Y" },
		{ ABS_HAT3X, "HAT3-X" },
		{ ABS_HAT3Y, "HAT3-Y" },
		{ ABS_MISC, "MISC" },
		{ ABS_INVALID, "-" }
	};
	
	const Util::NamedCapability Util::buttonNames[] = {
		{ BTN_NONE, "NONE" },
		{ BTN_JOYSTICK, "JOYSTICK" },
		{ BTN_TRIGGER, "TRIGGER" },
		{ BTN_THUMB, "THUMB" },
		{ BTN_THUMB2, "THUMB2" },
		{ BTN_TOP, "TOP" },
		{ BTN_TOP2, "TOP2" },
		{ BTN_PINKIE, "PINKIE" },
		{ BTN_BASE, "BASE" },
		{ BTN_BASE2, "BASE2" },
		{ BTN_BASE3, "BASE3" },
		{ BTN_BASE4, "BASE4" },
		{ BTN_BASE5, "BASE5" },
		{ BTN_BASE6, "BASE6" },
		{ BTN_BASE6 + 1, "BASE7" },
		{ BTN_BASE6 + 2, "BASE8" },
		{ BTN_BASE6 + 3, "BASE9" },
		{ BTN_DEAD, "DEAD" },
		{ BTN_A, "A" },
		{ BTN_B, "B" },
		{ BTN_C, "C" },
		{ BTN_X, "X" },
		{ BTN_Y, "Y" },
		{ BTN_Z, "Z" },
		{ BTN_SOUTH, "SOUTH" },
		{ BTN_EAST, "EAST" },
		{ BTN_NORTH, "NORTH" },
		{ BTN_WEST, "WEST" },
		{ BTN_GAMEPAD, "GAMEPAD" },
		{ BTN_TL, "L1" },
		{ BTN_TR, "R1" },
		{ BTN_TL2, "L2" },
		{ BTN_TR2, "R2" },
		{ BTN_SELECT, "SELECT" },
		{ BTN_START, "START" },
		{ BTN_MODE, "MODE" },
		{ BTN_THUMBL, "LEFT-STICK" },
		{ BTN_THUMBR, "RIGHT-STICK" },
		{ BTN_WHEEL, "WHEEL" },
		{ BTN_GEAR_DOWN, "GEAR-DOWN" },
		{ BTN_GEAR_UP, "GEAR-UP" },
		{ BTN_DPAD_UP, "DPAD-UP" },
		{ BTN_DPAD_DOWN, "DPAD-DOWN" },
		{ BTN_DPAD_LEFT, "DPAD-LEFT" },
		{ BTN_DPAD_RIGHT, "DPAD-RIGHT" },
		{ BTN_TRIGGER_HAPPY1, "HAPPY1" },
		{ BTN_TRIGGER_HAPPY2, "HAPPY2" },
		{ BTN_TRIGGER_HAPPY3, "HAPPY3" },
		{ BTN_TRIGGER_HAPPY4, "HAPPY4" },
		{ BTN_TRIGGER_HAPPY5, "HAPPY5" },
		{ BTN_TRIGGER_HAPPY6, "HAPPY6" },
		{ BTN_TRIGGER_HAPPY7, "HAPPY7" },
		{ BTN_TRIGGER_HAPPY8, "HAPPY8" },
		{ BTN_TRIGGER_HAPPY9, "HAPPY9" },
		{ BTN_TRIGGER_HAPPY10, "HAPPY10" },
		{ BTN_TRIGGER_HAPPY11, "HAPPY11" },
		{ BTN_TRIGGER_HAPPY12, "HAPPY12" },
		{ BTN_TRIGGER_HAPPY13, "HAPPY13" },
		{ BTN_TRIGGER_HAPPY14, "HAPPY14" },
		{ BTN_TRIGGER_HAPPY15, "HAPPY15" },
		{ BTN_TRIGGER_HAPPY16, "HAPPY16" },
		{ BTN_TRIGGER_HAPPY17, "HAPPY17" },
		{ BTN_TRIGGER_HAPPY18, "HAPPY18" },
		{ BTN_TRIGGER_HAPPY19, "HAPPY19" },
		{ BTN_TRIGGER_HAPPY20, "HAPPY20" },
		{ BTN_TRIGGER_HAPPY21, "HAPPY21" },
		{ BTN_TRIGGER_HAPPY22, "HAPPY22" },
		{ BTN_TRIGGER_HAPPY23, "HAPPY23" },
		{ BTN_TRIGGER_HAPPY24, "HAPPY24" },
		{ BTN_TRIGGER_HAPPY25, "HAPPY25" },
		{ BTN_TRIGGER_HAPPY26, "HAPPY26" },
		{ BTN_TRIGGER_HAPPY27, "HAPPY27" },
		{ BTN_TRIGGER_HAPPY28, "HAPPY28" },
		{ BTN_TRIGGER_HAPPY29, "HAPPY29" },
		{ BTN_TRIGGER_HAPPY30, "HAPPY30" },
		{ BTN_TRIGGER_HAPPY31, "HAPPY31" },
		{ BTN_TRIGGER_HAPPY32, "HAPPY32" },
		{ BTN_TRIGGER_HAPPY33, "HAPPY33" },
		{ BTN_TRIGGER_HAPPY34, "HAPPY34" },
		{ BTN_TRIGGER_HAPPY35, "HAPPY35" },
		{ BTN_TRIGGER_HAPPY36, "HAPPY36" },
		{ BTN_TRIGGER_HAPPY37, "HAPPY37" },
		{ BTN_TRIGGER_HAPPY38, "HAPPY38" },
		{ BTN_TRIGGER_HAPPY39, "HAPPY39" },
		{ BTN_TRIGGER_HAPPY40, "HAPPY40" },
		{ BTN_TRIGGER_HAPPY40 + 1, "HAPPY41" },
		{ BTN_TRIGGER_HAPPY40 + 2, "HAPPY42" },
		{ BTN_TRIGGER_HAPPY40 + 3, "HAPPY43" },
		{ BTN_TRIGGER_HAPPY40 + 4, "HAPPY44" },
		{ BTN_TRIGGER_HAPPY40 + 5, "HAPPY45" },
		{ BTN_TRIGGER_HAPPY40 + 6, "HAPPY46" },
		{ BTN_TRIGGER_HAPPY40 + 7, "HAPPY47" },
		{ BTN_TRIGGER_HAPPY40 + 8, "HAPPY48" },
		{ BTN_TRIGGER_HAPPY40 + 9, "HAPPY49" },
		{ BTN_TRIGGER_HAPPY40 + 10, "HAPPY50" },
		{ BTN_INVALID, "-" }
	};
	
	std::string Util::getAxisName(int axis) {
		for (int a = 0; axisNames[a].code != ABS_INVALID; a++) {
			if (axis == axisNames[a].code) {
				return axisNames[a].name;
			}
		}
		
		std::string result(16, '\0');
		int length = std::snprintf(result.data(), result.size(), "0x%X", axis);
		result.resize(length);
		return result;
	}
	
	std::string Util::getDeviceAxisName(int device, int axis) {		
		std::string name = getAxisName(axis);
		if (device < 0) {
			return name;
		}
		return std::to_string(device).append(":").append(name);
	}
	
	std::string Util::getButtonName(int button) {
		for (int b = 0; buttonNames[b].code != BTN_INVALID; b++) {
			if (button == buttonNames[b].code) {
				return buttonNames[b].name;
			}
		}
		std::string result(16, '\0');
		int length = std::snprintf(result.data(), result.length(), "0x%X", button);
		result.resize(length);
		return result;
	}
	
	std::string Util::getDeviceButtonName(int device, int button) {
		std::string name = getButtonName(button);
		if (device < 0) {
			return name;
		}
		return std::to_string(device).append(":").append(name);
	}
	
	bool Util::extractDeviceFromCapability(const std::string& capability, int& device, std::string& name) {
		size_t pos = capability.find(":");
		if (pos != std::string::npos) {
			try {
				device = std::stoul(capability.substr(0, pos));
			} catch (std::exception&) {
				return false;
			}
			name = capability.substr(pos + 1);
		} else {
			device = 0;
			name = capability;
		}
		return true;
	}
	
	bool Util::getAxisFromName(const std::string& name, int& axis) {
		std::string uppercase = name;
		std::transform(uppercase.begin(), uppercase.end(), uppercase.begin(), ::toupper);
		for (int a = 0; axisNames[a].code != ABS_INVALID; a++) {
			if (uppercase.compare(axisNames[a].name) == 0) {
				axis = axisNames[a].code;
				return true;
			}
		}
		{
			int a;
			if (sscanf(name.c_str(), "0x%x", &a) == 1) {
				axis = a;
				return true;
			}
		}
		return false;
	}
	
	bool Util::getButtonFromName(const std::string& name, int& button) {
		std::string uppercase = name;
		std::transform(uppercase.begin(), uppercase.end(), uppercase.begin(), ::toupper);
		for (int b = 0; buttonNames[b].code != BTN_INVALID; b++) {
			if (uppercase.compare(buttonNames[b].name) == 0) {
				button = buttonNames[b].code;
				return true;
			}
		}
		{
			int b;
			if (sscanf(name.c_str(), "0x%x", &b) == 1) {
				button = b;
				return true;
			}
		}
		return false;
	}
	
	std::string Util::getEffectTypeName(int type) {
		switch (type) {
			case FF_RUMBLE:
				return "RUMBLE";
			case FF_PERIODIC:
				return "PERIODIC";
			case FF_CONSTANT:
				return "CONSTANT";
			case FF_SPRING:
				return "SPRING";
			case FF_FRICTION:
				return "FRICTION";
			case FF_DAMPER:
				return "DAMPER";
			case FF_INERTIA:
				return "INERTIA";
			case FF_RAMP:
				return "RAMP";
			case FF_SQUARE:
				return "SQUARE";
			case FF_TRIANGLE:
				return "TRIANGLE";
			case FF_SINE:
				return "SINE";
			case FF_SAW_UP:
				return "SAW UP";
			case FF_SAW_DOWN:
				return "SAW DOWN";
			case FF_CUSTOM:
				return "CUSTOM";
			default:
				return "??";
		}
	}
}