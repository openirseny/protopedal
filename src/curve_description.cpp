#include "curve_description.h"



namespace proto {
	CurveDescription::CurveDescription() :
		points() {}
		
	CurveDescription::~CurveDescription() {}
	
	void CurveDescription::addSupport(float input, float output) {
		points.push_back({ input, output });
	}
	
	int CurveDescription::apply(int input, int inmin, int inmax, int outmin, int outmax, bool invert) {
		int range0 = inmax - inmin;
		int range1 = outmax - outmin;
		if (range0 <= 0 || range1 <= 0) {
			return invert ? outmax : outmin;
		}
		bool outside = points.size() < 2;
		if (input <= inmin) {
			input = inmin;
			outside = true;
		}
		if (input >= inmax) {
			input = inmax;
			outside = true;
		}
		int in0 = input - inmin;
		float out0 = in0;
		if (!outside) {
			for (unsigned int i = 1; i < points.size(); i++) {
				float rel0 = in0 - points[i].input*range0;
				// in/range > support/range
				if (rel0 < 0) {
					out0 = rel0*(points[i].output - points[i - 1].output)/(points[i].input - points[i - 1].input) + points[i].output*range0;
					break;
				}
			}
		}
		int offset1 = std::lroundf(out0);
		if (range0 != range1) {
			offset1 = std::lroundf((out0*range1)/range0);
		}
		int output = invert ? outmax - offset1 : outmin + offset1;
		return output;
	}
	
	void CurveDescription::clear() {
		points.clear();
	}
	
	void CurveDescription::normalize() {
		if (points.size() < 2) {
			return;
		}
		struct {
			bool operator()(Support a, Support b) {
				return a.input <= b.input;
			}
		} compare;
		std::sort(points.begin(), points.end(), compare);
		Support minimum = points[0];
		Support maximum = points[points.size() - 1];
		Support range = { maximum.input - minimum.input, maximum.output - minimum.output };
		for (Support& point : points) {
			point.input -= minimum.input;
			if (range.input > 0.01f) {
				point.input /= range.input;
			}
			point.output -= minimum.output;
			if (range.output > 0.01f) {
				point.output /= range.output;
			}
		}
	}
	
	void CurveDescription::loadCSV(const std::string& file) {
		clear();
		std::ifstream stream;
		stream.open(file, std::ios_base::in);
		if (!stream.is_open()) {
			std::string error = "File not found";
			error.append(file);
			throw std::runtime_error(error);
		}
		int errorCode = 0;
		int lineNumber = 1;
		std::string line;
		for (; std::getline(stream, line); lineNumber++) {
			size_t start = line.find('#');
			if (start != line.npos) {
				line = line.substr(0, start);
			}
			bool empty = true;
			for (unsigned int i = 0; i < line.length(); i++) {
				if (line[i] != ' ' && line[i] != '\t') {
					empty = false;
					break;
				}
			}
			if (empty) {
				continue;
			}
			float point[2] = { 0, 0 };
			start = 0;
			for (int i = 0; i < 2 && errorCode == 0; i++) {
				errorCode = 1;
				if (sscanf(line.c_str() + start, "%f", &point[i]) != 1) {
					break;
				}
				for (; start < line.length(); start++) {
					if (line[start] == ',' || line[start] == '\t' || line[start] == ' ') {
						errorCode = 0;
						break;
					}
				}
			}
			if (errorCode != 0) {
				break;
			}
			addSupport(point[0], point[1]);
		}
		stream.close();
		if (errorCode != 0) {
			clear();
			std::string error = "Invalid syntax in line ";
			error.append(std::to_string(lineNumber)).append(" ");
			error.append(line);
			throw std::runtime_error(error);
		}
		normalize();
	}
}