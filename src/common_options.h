#ifndef PROTO_COMMON_OPTIONS_H
#define PROTO_COMMON_OPTIONS_H

namespace proto {
	struct CommonOptions {
		int minimumMessagePriority;
		std::string ffbLog;
		bool daemonMode;
	};
}
#endif
