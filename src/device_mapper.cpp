#include "device_mapper.h"

namespace proto {

	DeviceMapper::DeviceMapper() {
	}

	DeviceMapper::~DeviceMapper() {
	}

	bool DeviceMapper::initialize(DeviceSetup& setup, std::vector<PhysicalDevice>& phys, VirtualDevice& virt) {
		if (!virt.beginOpen()) {
			return false;
		}
		PhysicalDevice* primary = nullptr;
		PhysicalDevice* ffb = nullptr;
		for (PhysicalDevice& dev : phys) {
			if (!initializePhys(setup, dev)) {
				if (setup.deviceRequired.at(dev.getIndex())) {
					return false;
				}
			} else {
				if (primary == nullptr) {
					primary = &dev;
				}
				if (dev.getIndex() == setup.ffbDevice) {
					ffb = &dev;
				}
			}
		}
		if (primary == nullptr) {
			Logging::beginError(8) << "No device available. Need at least one for initialization";
			Logging::endError(8);
			return false;
		}
		if (!initializeParams(setup, *primary, virt)) {
			return false;
		}
		if (!initializeAxes(setup, phys, virt)) {
			return false;
		}
		if (!initializeButtons(setup, phys, virt)) {
			return false;
		}
		if (ffb != nullptr) {
			if (!initializeFFBEffects(setup, *ffb, virt)) {
				return false;
			}
		}
		if (!virt.finishOpen()) {
			return false;
		}
		if (ffb != nullptr) {
			if (!beginFFBEffects(setup, *ffb)) {
				return false;
			}
		}
		for (auto pair : setup.capabilityMap) {
			EventPlugin* plugin = pair.second.plugin;
			if (plugin != nullptr) {
				const DeviceSetup::EventSource& source = pair.first;
				const DeviceSetup::EventMap& mapping = pair.second;
				std::ostream& message = Logging::beginMessage(8) << "Initializing plugin";
				int device = phys.size() < 2 ? -1 : source.device;
				switch (source.type) {
				case EV_KEY:
					message << " for button ";
					message << Util::getDeviceButtonName(device, source.code);
					break;
				case EV_ABS:
					message << " for axis ";
					message << Util::getDeviceAxisName(device, source.code);
					break;
				}
				if (mapping.args.length() > 0) {
					message << " with " << mapping.args;
				}
				Logging::endMessage(8);
				try {
					plugin->init(setup, pair.second);
				} catch (std::exception& e) {
					Logging::beginError(8) << e.what();
					Logging::endError(8);
					return false;
				}
			}
		}
		return true;
	}
	
	bool DeviceMapper::initializePhys(DeviceSetup& setup, PhysicalDevice& phys) {
		if (!phys.open(setup.devicePaths.at(phys.getIndex()))) {
			return false;
		}
		std::ostream& log = Logging::beginMessage(1) << "Advertised event types:";
		if (phys.hasEvent(EV_SYN)) {
			log << " SYN";
		}
		if (phys.hasEvent(EV_KEY)) {
			log << " KEY";
		}
		if (phys.hasEvent(EV_REL)) {
			log << " REL";
		}
		if (phys.hasEvent(EV_ABS)) {
			log << " ABS";
		}
		if (phys.hasEvent(EV_MSC)) {
			log << " MSC";
		}
		if (phys.hasEvent(EV_SW)) {
			log << " SW";
		}
		if (phys.hasEvent(EV_LED)) {
			log << " LED";
		}
		if (phys.hasEvent(EV_SND)) {
			log << " SND";
		}
		if (phys.hasEvent(EV_REP)) {
			log << " REP";
		}
		if (phys.hasEvent(EV_FF)) {
			log << " FF";
		}
		if (phys.hasEvent(EV_FF_STATUS)) {
			log << " FF_STATUS";
		}
		if (phys.hasEvent(EV_PWR)) {
			log << " PWR";
		}
		Logging::endMessage(1);
		
		if (!phys.readSetup()) {
			phys.close();
			return false;
		}
		if (!phys.setExclusiveAccess(setup.exclusiveAccess)) {
			if (setup.exclusiveAccess) {
				Logging::lineMessage(4, "Failed to grab device");
			} else {
				Logging::lineMessage(4, "Failed to ungrab device");
			}
		}
		return true;
	}
	
	bool DeviceMapper::initializeParams(DeviceSetup& setup, PhysicalDevice& phys, VirtualDevice& virt) {
		std::ostream& message = Logging::beginMessage(4);
		message << "Setting up device ";
		if (setup.name.length() == 0) {
			setup.name = phys.getName();
		} 
		virt.setName(setup.name);
		message << setup.name << " (";
		bool customVendor = true;
		if (setup.vendor == 0) {
			setup.vendor = phys.getVendor();
			customVendor = false;
		} 
		virt.setVendor(setup.vendor);
		message << std::hex << std::uppercase << std::setw(4) << std::setfill('0');
		message << setup.vendor << ":";
		
		if (setup.product == 0) {
			setup.product = phys.getProduct();
			if (!customVendor) {
				if (setup.product < 0xFFFF) {
					setup.product += 1;
				} else {
					setup.product -= 1;
				}
			}
		}
		virt.setProduct(setup.product);
		message << std::hex << std::uppercase << std::setw(4) << std::setfill('0');
		message << setup.product << ")";
		message << std::dec;
		Logging::endMessage(4);
		return true;
	}

	bool DeviceMapper::initializeAxes(DeviceSetup& setup, std::vector<PhysicalDevice>& phys, VirtualDevice& virt) {
		Backend::AbsoluteBitmap sourceCovered = {};
		Backend::AbsoluteBitmap targetCovered = {};
		// axis -> axis
		for (auto pair = setup.capabilityMap.begin(); pair != setup.capabilityMap.end(); pair++) {
			DeviceSetup::EventSource source = pair->first;
			if (source.type != EV_ABS) {
				continue; // button -> axis handeled below
			}
			if (source.code >= ABS_MIN && source.code <= ABS_MAX) {
				Backend::setMapBit(&sourceCovered, source.code);
			}
			DeviceSetup::EventMap& mapping = pair->second;
			if (mapping.type != EV_ABS) {
				continue;
			}
			DeviceSetup::CapabilityKey key = DeviceSetup::toCapability(mapping);
			auto iter = setup.capabilitySetup.find(key);
			if (iter == setup.capabilitySetup.end()) {
				// the setup entry should always exist (set by option parser)
				// so we should always get past here
				continue; 
			}
			DeviceSetup::CapabilitySetup& targetSetup = iter->second;
			input_absinfo sourceInfo = {};
			sourceInfo.minimum = sourceInfo.maximum = INT_MIN;
			bool sourceValid = source.device < (int)phys.size();
			if (sourceValid) {
				PhysicalDevice& dev = phys.at(source.device);
				sourceValid = dev.getAxisInfo(source.code, sourceInfo);
			}
			std::ostream& message = Logging::beginMessage(7) << "Mapping ";
			if (!sourceValid && source.code >= ABS_MIN) {
				message << "missing ";
			}
			message << "axis " << Util::getDeviceAxisName(phys.size() < 2 ? -1 : source.device, source.code) << " to " << Util::getAxisName(mapping.code);
			DeviceSetup::AxisSetup fallback = { 0, 0xFF };
			DeviceSetup::completeRanges(sourceInfo, mapping, targetSetup.axis, fallback);
			message << ", range " << mapping.minimum << " to " << mapping.maximum;
			if (mapping.invert) {
				message << ", inverted";
			}
			Logging::endMessage(7);
			if (mapping.code >= ABS_MIN && mapping.code <= ABS_MAX) {
				input_absinfo targetInfo = {};
				targetInfo.minimum = targetSetup.axis.minimum;
				targetInfo.maximum = targetSetup.axis.maximum;
				virt.addAxis(mapping.code, targetInfo);
				Backend::setMapBit(&targetCovered, mapping.code); 
			}
			// axis->button handeled in button init
		}
		// button -> axis
		for (auto pair = setup.capabilityMap.begin(); pair != setup.capabilityMap.end(); pair++) {
			DeviceSetup::EventSource source = pair->first;
			if (source.type != EV_KEY) {
				continue;
			}
			// setting the covered bit is not necessary here
			// this info will be evaluated (and the bit set) during the button setup again
			DeviceSetup::EventMap& mapping = pair->second;
			if (mapping.type != EV_ABS) {
				continue;
			}
			DeviceSetup::CapabilityKey key = DeviceSetup::toCapability(mapping);
			auto iter = setup.capabilitySetup.find(key);
			if (iter == setup.capabilitySetup.end()) {
				continue;
			}
			DeviceSetup::CapabilitySetup& targetSetup = iter->second;
			bool sourceValid = source.device < (int)phys.size();
			if (sourceValid) {
				PhysicalDevice& dev = phys.at(source.device);
				sourceValid = dev.hasButton(source.code);
			}
			std::ostream& message = Logging::beginMessage(7) << "Mapping ";
			if (!sourceValid && source.code >= BTN_MIN) {
				message << "missing ";
			}
			message << "button " << Util::getDeviceButtonName(phys.size() < 2 ? -1 : source.device, source.code) << " to axis " << Util::getAxisName(mapping.code);
			input_absinfo sourceInfo = {};
			sourceInfo.minimum = 0;
			sourceInfo.maximum = 1;
			DeviceSetup::AxisSetup fallback = DeviceSetup::toAxisSetup(sourceInfo);
			DeviceSetup::completeRanges(sourceInfo, mapping, targetSetup.axis, fallback);
			message << ", range " << mapping.minimum << " to " << mapping.maximum;
			if (mapping.invert) {
				message << ", inverted";
			}
			Logging::endMessage(7);
			if (mapping.code >= ABS_MIN && mapping.code <= ABS_MAX) {
				input_absinfo targetInfo  = {};
				targetInfo.minimum = targetSetup.axis.minimum;
				targetInfo.maximum = targetSetup.axis.maximum;
				virt.addAxis(mapping.code, targetInfo);
				Backend::setMapBit(&targetCovered, mapping.code);
			}
		}
		
		
		// map non-specified axes to their direct counterpart
		unsigned int coveredAxisNo = 0;
		for (int a = ABS_MIN; a < ABS_MAX; a++) {
			if (Backend::isBitSet(&targetCovered, a)) {
				coveredAxisNo += 1;
			}
		}
		for (size_t i = 0; i < phys.size(); i++) {
			for (int a = ABS_MIN; a < ABS_MAX && coveredAxisNo < setup.maxAxisNo; a++) {
				struct input_absinfo sourceInfo;
				if (Backend::isBitSet(&sourceCovered, a) || Backend::isBitSet(&targetCovered, a) ||
					!phys[i].getAxisInfo(a, sourceInfo)) {
					continue;
				}
				DeviceSetup::CapabilitySetup targetSetup;
				targetSetup.axis = { sourceInfo.minimum, sourceInfo.maximum };
				DeviceSetup::CapabilityKey key = { EV_ABS, a };
				setup.capabilitySetup[key] = targetSetup;
				DeviceSetup::EventMap mapping = { EV_ABS, a, sourceInfo.minimum, sourceInfo.maximum, false, nullptr };
				DeviceSetup::EventSource source = { (int)i, EV_ABS, a };
				setup.capabilityMap.insert({ source, mapping });
				input_absinfo targetInfo = {};
				targetInfo.minimum = targetSetup.axis.minimum;
				targetInfo.maximum = targetSetup.axis.maximum;
				std::ostream& message = Logging::beginMessage(7);
				message << "Mapping axis " << Util::getDeviceAxisName(phys.size() < 2 ? -1 : i, a) << " to " << Util::getAxisName(a);
				message << ", range " << mapping.minimum << " to " << mapping.maximum;
				Logging::endMessage(7);
				virt.addAxis(a, targetInfo);
				Backend::setMapBit(&sourceCovered, a);
				Backend::setMapBit(&targetCovered, a);
				coveredAxisNo += 1;
			}
		}
		// add more dummy axes if requested
		for (int a = ABS_MIN; a < ABS_MAX && coveredAxisNo < setup.minAxisNo; a++) {
			if (!Backend::isBitSet(&targetCovered, a)) {
				DeviceSetup::CapabilitySetup targetSetup;
				targetSetup.axis = { 0, 0xFFFF };
				DeviceSetup::CapabilityKey key = { EV_ABS, a };
				setup.capabilitySetup[key] = targetSetup;
				input_absinfo targetInfo = {};
				targetInfo.minimum = targetSetup.axis.minimum;
				targetInfo.maximum = targetSetup.axis.maximum;
				virt.addAxis(a, targetInfo);
				Backend::setMapBit(&targetCovered, a);
				Logging::beginMessage(7) << "Adding axis " << Util::getAxisName(a);
				Logging::endMessage(7);
				coveredAxisNo += 1;
			}
		}
		return true;
	}

	bool DeviceMapper::initializeButtons(DeviceSetup& setup, std::vector<PhysicalDevice>& phys, VirtualDevice& virt) {
		Backend::KeyBitmap sourceCovered = {};
		Backend::KeyBitmap targetCovered = {};
		// button -> button
		for (auto pair = setup.capabilityMap.begin(); pair != setup.capabilityMap.end(); pair++) {
			DeviceSetup::EventSource source = pair->first;
			if (source.type != EV_KEY) {
				continue;
			}
			if (source.code >= BTN_MIN && source.code <= BTN_MAX) {
				Backend::setMapBit(&sourceCovered, source.code);
			}
			DeviceSetup::EventMap mapping = pair->second;
			if (mapping.type != EV_KEY) {
				continue;
			}
			DeviceSetup::CapabilityKey key = DeviceSetup::toCapability(mapping);
			auto iter = setup.capabilitySetup.find(key);
			if (iter == setup.capabilitySetup.end()) {
				continue;
			}
			bool sourceValid = source.device < (int)phys.size();
			if (sourceValid) {
				PhysicalDevice& dev = phys.at(source.device);
				sourceValid = dev.hasButton(source.code);
			}
				
			DeviceSetup::CapabilitySetup& targetSetup = iter->second;
			std::ostream& message = Logging::beginMessage(7) << "Mapping ";
			if (!sourceValid && source.code >= BTN_MIN) {
				message << "missing ";
			}
			message << "button " << Util::getDeviceButtonName(phys.size() < 2 ? -1 : source.device, source.code) << " to " << Util::getButtonName(mapping.code);
			if (targetSetup.button.toggle) {
				message << " as toggle switch";
			}
			if (mapping.invert) {
				message << ", inverted";
			}
			Logging::endMessage(7);
			if (mapping.code >= BTN_MIN && mapping.code <= BTN_MAX) {
				virt.addButton(mapping.code);
				Backend::setMapBit(&targetCovered, mapping.code);
			}
		}
		// axis -> button
		for (auto pair = setup.capabilityMap.begin(); pair != setup.capabilityMap.end(); pair++) {
			DeviceSetup::EventSource source = pair->first;
			if (source.type != EV_ABS) {
				continue;
			}
			DeviceSetup::EventMap& mapping = pair->second;
			if (mapping.type != EV_KEY) {
				continue;
			}
			DeviceSetup::CapabilityKey key = DeviceSetup::toCapability(mapping);
			auto iter = setup.capabilitySetup.find(key);
			if (iter == setup.capabilitySetup.end()) {
				continue;
			}
			input_absinfo sourceInfo = {};
			sourceInfo.minimum = sourceInfo.maximum = INT_MIN;
			bool sourceValid = source.device < (int)phys.size();
			if (sourceValid) {
				PhysicalDevice& dev = phys.at(source.device);
				sourceValid = dev.getAxisInfo(source.code, sourceInfo);
			}
			DeviceSetup::AxisSetup dummySetup = { INT_MIN, INT_MIN };
			DeviceSetup::AxisSetup fallback = { 0, 0xFF };
			DeviceSetup::completeRanges(sourceInfo, mapping, dummySetup, fallback);
			std::ostream& message = Logging::beginMessage(7) << "Mapping ";
			if (!sourceValid && source.code >= ABS_MIN) {
				message << "missing ";
			}
			message << "axis " << Util::getDeviceAxisName(phys.size() < 2 ? -1 : source.device, source.code) << " to button " << Util::getButtonName(mapping.code);
			message << ", range " << mapping.minimum << " to " << mapping.maximum;
			DeviceSetup::CapabilitySetup& targetSetup = iter->second;
			if (targetSetup.button.toggle) {
				// TODO drop toggle support entirely?
				//message << " as toggle switch";
			}
			if (mapping.invert) {
				message << ", inverted";
			}
			Logging::endMessage(7);
			if (mapping.code >= BTN_MIN && mapping.code <= BTN_MAX) {
				virt.addButton(mapping.code);
				Backend::setMapBit(&targetCovered, mapping.code);
			}
		}
		// map to direct counterpart
		unsigned int coveredButtonNo = 0;
		for (int b = BTN_MIN; b < BTN_MAX; b++) {
			if (Backend::isBitSet(&targetCovered, b)) {
				coveredButtonNo += 1;
			}
		}
		for (size_t i = 0; i < phys.size(); i++) {
			for (int b = BTN_MIN; b <= BTN_MAX && coveredButtonNo < setup.maxButtonNo; b++) {
				if (!Backend::isBitSet(&sourceCovered, b) && !Backend::isBitSet(&targetCovered, b) && phys.at(i).hasButton(b)) {
					virt.addButton(b);
					DeviceSetup::EventMap targetInfo = { EV_KEY, b };
					DeviceSetup::EventSource source = { (int)i, EV_KEY, b };
					setup.capabilityMap.insert({ source, targetInfo });
					DeviceSetup::CapabilitySetup targetSetup;
					targetSetup.button = { false };
					DeviceSetup::CapabilityKey key = DeviceSetup::toCapability(targetInfo);
					setup.capabilitySetup[key] = targetSetup;
					Backend::setMapBit(&sourceCovered, b);
					Backend::setMapBit(&targetCovered, b);
					Logging::beginMessage(7) << "Mapping button " << Util::getDeviceButtonName(phys.size() < 2 ? -1 : i, b) << " to " << Util::getButtonName(b);
					Logging::endMessage(7);
					coveredButtonNo += 1;
				}
			}
		}
		// add more dummy buttons if requested
		for (int b = BTN_MIN; b <= BTN_MAX && coveredButtonNo < setup.minButtonNo ; b++) {
			if (!Backend::isBitSet(&targetCovered, b)) {
				virt.addButton(b);
				DeviceSetup::CapabilitySetup targetSetup;
				targetSetup.button = { false };
				DeviceSetup::CapabilityKey key = { EV_KEY, b };
				setup.capabilitySetup[key] = targetSetup;
				Backend::setMapBit(&targetCovered, b);
				Logging::beginMessage(7) << "Adding button " << Util::getButtonName(b);
				Logging::endMessage(7);
				coveredButtonNo += 1;
			}
		}
		return true;
	}
	
	bool DeviceMapper::initializeFFBEffects(DeviceSetup& setup, PhysicalDevice& phys, VirtualDevice& virt) {
		int effectNo = phys.getFFBEffectNo();
		if (effectNo == 0) {
			return true;
		}
		virt.enableFFB(effectNo);
		std::ostream& message = Logging::beginMessage(7) << "Supporting " << effectNo << " FFB effects";
		message << " on device " << setup.ffbDevice;
		Logging::endMessage(7);
		virt.enableFFB(effectNo);
		for (int e = FF_EFFECT_MIN; e < FF_WAVEFORM_MAX; e++) {
			if (phys.hasFFBEffect(e)) {
				Logging::appendMessage(7, "Enabling effect type ");
				Logging::lineMessage(7, Util::getEffectTypeName(e));
				virt.addFFBEffect(e);
			}
		}
		return true;
	}
	
	bool DeviceMapper::beginFFBEffects(DeviceSetup& setup, PhysicalDevice& phys) {
		if (setup.fixedGain > -1) {
			FFBEvent event(setup.fixedGain, true);
			phys.sendEvent(&event);
		}
		if (setup.fixedAutoCenter > -1) {
			FFBEvent event(setup.fixedAutoCenter, false);
			phys.sendEvent(&event);
		}
		return true;
	}
	
	void DeviceMapper::finish(DeviceSetup& setup, std::vector<PhysicalDevice>& phys, VirtualDevice& virt) {
		finishVirt(setup, virt);
		for (PhysicalDevice& dev : phys) {
			finishPhys(setup, dev);
		}
		for (auto pair : setup.capabilityMap) {
			delete pair.second.curve;
			if (pair.second.plugin != nullptr) {
				pair.second.plugin->quit(setup, pair.second);
				pair.second.plugin->unload();
				delete pair.second.plugin;
				pair.second.plugin = nullptr;
			}
		}
		setup.capabilityMap.clear();
		setup.capabilitySetup.clear();
	}
	
	void DeviceMapper::finishVirt(DeviceSetup& setup, VirtualDevice& virt) {
		virt.close();
	}
	
	void DeviceMapper::finishPhys(DeviceSetup& setup, PhysicalDevice& phys) {
		if (setup.reportAxisLimits) {
			for (int a = ABS_MIN; a <= ABS_MAX; a++) {
				int minimum = 0;
				int maximum = 0;
				if (phys.getAxisLimits(a, minimum, maximum)) {
					std::ostream& stream = Logging::beginMessage(8) << "Axis ";
					stream << Util::getAxisName(a) << " moved between ";
					stream << minimum << " and " << maximum;
					Logging::endMessage(8);
				}
			}
		}
		if (setup.reportButtonLimits) {
			for (int b = BTN_MIN; b <= BTN_MAX; b++) {
				int minimum = 0;
				int maximum = 0;
				if (phys.getButtonLimits(b, minimum, maximum)) {
					std::ostream& stream = Logging::beginMessage(8) << "Button ";
					stream << Util::getButtonName(b) << " clicked";
					Logging::endMessage(8);
				}
			}
		}
		phys.close();
	}

	bool DeviceMapper::continueMapping(DeviceSetup& setup, std::vector<PhysicalDevice>& phys, VirtualDevice& virt) {
		PhysicalDevice::collectEvents(phys);
		for (size_t i = 0; i < phys.size(); i++) {
			PhysicalDevice& dev = phys.at(i);
			while (DeviceEvent* event = dev.nextEvent()) {
				mapEvent(setup, dev, event, virt, true);
				dev.feedbackEvent(event);
			}
		}
		{ // plugin idling
			for (auto pair : setup.capabilityMap) {
				if (pair.second.plugin != nullptr) {
					IdleEvent idle(pair.first.device);
					PhysicalDevice& dev = phys[pair.first.device];
					mapThroughPlugin(setup, dev, pair.second, &idle, virt);
				}
			}
		}
		virt.collectEvents();
		while (DeviceEvent* event = virt.nextEvent()) {
			if (setup.ffbDevice < 0 || (size_t)setup.ffbDevice >= phys.size()) {
				virt.feedbackEvent(event);
				continue;
			}
			PhysicalDevice& dev = phys.at(setup.ffbDevice);
			switch (event->getType()) {
				case EV_FF:
					if (FFBEvent* source = dynamic_cast<FFBEvent*>(event)) {
						FFBEvent target(*source);
						if (mapFFBtoFFBEvent(setup, *source, target, virt)) {
							dev.sendEvent(&target);
						}
					}
					break;
				default:
					dev.sendEvent(event);
					break;
			}
			virt.feedbackEvent(event);
		}
		return true;
	}
	
	bool DeviceMapper::mapEvent(DeviceSetup& setup, PhysicalDevice& phys, DeviceEvent* event, VirtualDevice& virt, bool enablePlugins) {
		int deviceIndex = event->getDevice();
		switch (event->getType()) {
			case EV_SYN:
				virt.sendEvent(event);
				break;
			case EV_ABS: {
				if (AxisEvent* source = dynamic_cast<AxisEvent*>(event)) {
					DeviceSetup::EventSource key = { deviceIndex, EV_ABS, source->getAxis() };
					auto range = setup.capabilityMap.equal_range(key);
					for (auto iter = range.first; iter != range.second; iter++) {
						if (enablePlugins && iter->second.plugin != nullptr) {
							if (!mapThroughPlugin(setup, phys, iter->second, source, virt)) {
								continue;
							}
						}
						switch (iter->second.type) {
							case EV_ABS: {
								AxisEvent target(deviceIndex, iter->second.code, {});
								if (mapAxisToAxisEvent(setup, *source, target, iter->second, virt)) {
									virt.sendEvent(&target);
								}
								break;
							}
							case EV_KEY: {
								ButtonEvent target(deviceIndex, iter->second.code, {});
								if (mapAxisToButtonEvent(setup, *source, target, iter->second, virt)) {
									virt.sendEvent(&target);
								}
								break;
							}
							default:
								break;
						}
					}
				}
				break;
			}
			case EV_KEY: {
				if (ButtonEvent* source = dynamic_cast<ButtonEvent*>(event)) {
					DeviceSetup::EventSource key = { source->getDevice(), EV_KEY, source->getButton() };
					auto range = setup.capabilityMap.equal_range(key);
					for (auto iter = range.first; iter != range.second; iter++) {
						if (enablePlugins && iter->second.plugin != nullptr) {
							if (!mapThroughPlugin(setup, phys, iter->second, source, virt)) {
								continue;
							}
						}
						switch (iter->second.type) {
							case EV_KEY: {
								ButtonEvent target(deviceIndex, iter->second.code, source->isPressed());
								if (mapButtonToButtonEvent(setup, *source, target, iter->second, virt)) {
									virt.sendEvent(&target);
								}
								break;
							}
							case EV_ABS: {
								AxisEvent target(deviceIndex, iter->second.code, {});
								if (mapButtonToAxisEvent(setup, *source, target, iter->second, virt)) {
									virt.sendEvent(&target);
								}
								break;
							}
							default:
								break;
						}
					}
				}
				break;
			}
			default:
				Logging::appendMessage(4, "Unmappable event ");
				Logging::lineMessage(4, std::to_string(event->getType()));
				break;
		}
		return true;
	}
	
	bool DeviceMapper::mapThroughPlugin(DeviceSetup& setup, PhysicalDevice& phys, DeviceSetup::EventMap& mapping, DeviceEvent* event, VirtualDevice& virt) {
		if (mapping.plugin == nullptr) {
			return true;
		}
		int more = 1;
		bool consumed = false;
		for (int c = 0; more > 0; c++) {
			char target[std::max(sizeof(ButtonEvent), sizeof(AxisEvent))];
			memset(target, 0, sizeof(target));
			new (target) IdleEvent(0);
			more = mapping.plugin->map(setup, mapping, event, (DeviceEvent*)target, c);
			if (more > 0) {
				mapEvent(setup, phys, (DeviceEvent*)&target, virt, false);
				more -= 1;
				consumed = true;
			}
		}
		return !consumed;
	}
	
	bool DeviceMapper::mapFFBtoFFBEvent(DeviceSetup& setup, FFBEvent& source, FFBEvent& target, VirtualDevice& virt) {
		switch (source.getPayloadType()) {
			case FFBEvent::PayloadType::GAIN:
				if (setup.fixedGain > -1) {
					*target.getGainPayload() = setup.fixedGain;
				}
				break;
			case FFBEvent::PayloadType::AUTOCENTER:
				if (setup.fixedAutoCenter > -1) {
					*target.getAutoCenterPayload() = setup.fixedAutoCenter;
				}
				break;
			default:
				break;
		}
		return true;
	}

	bool DeviceMapper::mapAxisToAxisEvent(DeviceSetup& setup, AxisEvent& source, AxisEvent& target, DeviceSetup::EventMap& mapping, VirtualDevice& virt) {
		DeviceSetup::CapabilityKey key = DeviceSetup::toCapability(mapping);
		auto iter = setup.capabilitySetup.find(key);
		if (iter == setup.capabilitySetup.end()) {
			return false;
		}
		DeviceSetup::CapabilitySetup& targetSetup = iter->second;
		input_absinfo targetInfo = {};
		targetInfo.minimum = targetSetup.axis.minimum;
		targetInfo.maximum = targetSetup.axis.maximum;
		if (mapping.curve != nullptr) {
			targetInfo.value = mapping.curve->apply(source.getValue(), mapping.minimum, mapping.maximum, targetInfo.minimum, targetInfo.maximum, mapping.invert);
		} else {
			targetInfo.value = DeviceSetup::mapToRange(source.getValue(), mapping.minimum, mapping.maximum, targetInfo.minimum, targetInfo.maximum, mapping.invert);
		}
		target = AxisEvent(source.getDevice(), mapping.code, targetInfo);
		return true;
	}
	
	bool DeviceMapper::mapAxisToButtonEvent(DeviceSetup& setup, AxisEvent& source, ButtonEvent& target, DeviceSetup::EventMap& mapping, VirtualDevice& virt) {
		DeviceSetup::CapabilityKey key = DeviceSetup::toCapability(mapping);
		auto iter = setup.capabilitySetup.find(key);
		if (iter == setup.capabilitySetup.end()) {
			return false;
		}
		int mid2 = mapping.maximum + mapping.minimum;
		bool pressed;
		if (mapping.curve != nullptr) {
			int curved2 = mapping.curve->apply(source.getValue(), mapping.minimum, mapping.maximum, mapping.minimum, mapping.maximum, false)*2;
			pressed = mapping.invert ? curved2 < mid2 : curved2 > mid2;
		} else {
			int source2 = source.getValue()*2;
			pressed = mapping.invert ? source2 < mid2 : source2 > mid2;
		}
		bool previous = virt.getButtonState(mapping.code);
		if (pressed == previous) {
			return false;
		}
		target = ButtonEvent(source.getDevice(), mapping.code, pressed);
		return true;
	}
	
	bool DeviceMapper::mapButtonToButtonEvent(DeviceSetup& setup, ButtonEvent& source, ButtonEvent& target, DeviceSetup::EventMap& mapping, VirtualDevice& virt) {
		DeviceSetup::CapabilityKey key = DeviceSetup::toCapability(mapping);
		auto iter = setup.capabilitySetup.find(key);
		if (iter == setup.capabilitySetup.end()) {
			return false;
		}
		bool pressed = mapping.invert ? !source.isPressed() : source.isPressed();
		target = ButtonEvent(source.getDevice(), target.getButton(), pressed);
		return true;
	}
	
	bool DeviceMapper::mapButtonToAxisEvent(DeviceSetup& setup, ButtonEvent& source, AxisEvent& target, DeviceSetup::EventMap& mapping, VirtualDevice& virt) {
		DeviceSetup::CapabilityKey key = DeviceSetup::toCapability(mapping);
		auto iter = setup.capabilitySetup.find(key);
		if (iter == setup.capabilitySetup.end()) {
			return false;
		}
		DeviceSetup::CapabilitySetup& targetSetup = iter->second;
		input_absinfo targetInfo = {};
		targetInfo.minimum = targetSetup.axis.minimum;
		targetInfo.maximum = targetSetup.axis.maximum;
		int sourceValue = source.isPressed() ? 1 : 0;
		if (mapping.curve != nullptr) {
			targetInfo.value = mapping.curve->apply(sourceValue, mapping.minimum, mapping.maximum, targetInfo.minimum, targetInfo.maximum, mapping.invert);
		} else {
			targetInfo.value = DeviceSetup::mapToRange(sourceValue, mapping.minimum, mapping.maximum, targetInfo.minimum, targetInfo.maximum, mapping.invert);
		}
		target = AxisEvent(source.getDevice(), mapping.code, targetInfo);
		return true;
	}
}
