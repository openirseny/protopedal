#include "idle_event.h"

namespace proto {
	
	IdleEvent::IdleEvent(int device) : 
		device(device) {
		
	}
	
	int IdleEvent::getType() {
		return EV_IDLE;
	}
	
	int IdleEvent::getDevice() {
		return device;
	}
	
}