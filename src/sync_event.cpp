#include "sync_event.h"

namespace proto {
	SyncEvent::SyncEvent(int report) : DeviceEvent(),
		report(report) {
	}

	SyncEvent::~SyncEvent() {
	}

	int SyncEvent::getType() {
		return EV_SYN;
	}
	
	int SyncEvent::getDevice() {
		return 0;
	}
	
	int SyncEvent::getReport() {
		return report;
	}
}