#ifndef PROTO_AXIS_EVENT_H
#define PROTO_AXIS_EVENT_H

#include <algorithm>

#include <linux/input.h>

#include "device_event.h"

namespace proto {

class AxisEvent : public DeviceEvent {
	private:
		int device;
		int axis;
		input_absinfo info;
	public:
		AxisEvent(int device, int axis, input_absinfo info);
		~AxisEvent();
	public:
		int getType() override;
		int getDevice() override;
		int getAxis();
		int getMinimum();
		int getMaximum();
		int getRange();
		int getValue();
	};

}

#endif // PROTO_AXIS_EVENT_H
