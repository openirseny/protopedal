#ifndef PROTO_LOGGING_H
#define PROTO_LOGGING_H

#include <string>
#include <iostream>
#include <fstream>
#include <chrono>
#include <iomanip>
#include "util.h"

namespace proto
{

	class Logging
	{
	public:
		static const int MESSAGE = 1;
		static const int ERROR = 2;
		static const int FILE = 3;
	private:
		static std::ostream discard;
		static std::ofstream logFile;
		static int minimumFilePriority;
		static int minimumMessagePriority;
		static int minimumErrorPriority;
		static std::chrono::time_point<std::chrono::steady_clock> initTime;
	public:
		static void init(const std::string& file);
		static void quit();
		static void setMinimumFilePriority(int priority);
		static void setMinimumMessagePriority(int priority);
		static void setMinimumErrorPriority(int priority);
	public:
		static void lineMessage(int priority, const std::string& message);
		static void lineError(int priority, const std::string& message);
		static void lineFile(int priority, const std::string& message);
		static void appendMessage(int priority, const std::string& message);
		static void appendError(int priority, const std::string& message);
		static std::ostream& appendFile(int priority, const std::string& message);
		static std::ostream& beginMessage(int priority, bool timestamp=false);
		static std::ostream& beginError(int priority, bool timestamp=false);
		static std::ostream& beginFile(int priority, bool timestamp=true);
		static void endMessage(int priority);
		static void endError(int priority);
		static void endFile(int priority);
		static long milliSecondsSinceInit();
	private:
		static void appendTimestamp(std::ostream& stream);
	};

}

#endif // PROTO_LOGGING_H
