#ifndef PROTO_EVENT_PLUGIN_H
#define PROTO_EVENT_PLUGIN_H

#include <dlfcn.h>
#include <string>
#include <stdexcept>
#include <vector>
#include "device_event.h"
#include "device_setup.h"

namespace proto {
	
	class EventPlugin {
	private:
		void* lib;
		void* initSym;
		void* quitSym;
		void* mapSym;
	public:
		EventPlugin();
		EventPlugin(const EventPlugin& other) = delete;
		~EventPlugin();
	public:
		void init(DeviceSetup& setup, DeviceSetup::EventMap& mapping);
		void quit(DeviceSetup& setup, DeviceSetup::EventMap& mapping);
		int map(DeviceSetup& setup, DeviceSetup::EventMap& mapping, DeviceEvent* source, DeviceEvent* target, int call);
		void loadSO(const std::string& file);
		void unload();
	private:
		static void splitArgs(const std::string& args, std::vector<std::string>& split);
	};
}

#endif
