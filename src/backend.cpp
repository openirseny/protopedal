#include "backend.h"

namespace proto {


	int Backend::openRoot() {
		auto candidates = {
			"/dev/uinput"
		};
		std::string errors;
		for (const char* path : candidates) {
			int file = open(path, O_RDWR);
			if (file >= 0) {
				return file;
			} else {
				errors.append(path);
				switch (errno) {
					case EACCES:
						errors.append(" is not accessible\n");
					break;
					case ENOENT:
						errors.append(" does not exist\n");;
					break;
					default:
						errors.append(" cannot be opened (").append(std::to_string(errno)).append(")\n");
					break;
				}
			}
		}
		throw std::runtime_error(errors);
	}

	Backend::Device Backend::beginDeviceSetup(int root) {
		Backend::Device device = {};
		device.file = root;
		device.opened = true;
		device.setup.id.bustype = BUS_USB;
		device.setup.id.product = 0x1111;
		device.setup.id.vendor = 0x1111;
		strcpy(device.setup.name, "Protopedal");
		device.pendingCapacity = sizeof(device.pendingEvents)/sizeof(device.pendingEvents[0]);
		return device;
	}

	void Backend::finishDeviceSetup(Device& device) {
		int errorCode = ioctl(device.file, UI_DEV_SETUP, &device.setup);
		if (errorCode != 0) {
			if (errno == EINVAL) {
				strncpy(device.dev.name, device.setup.name, sizeof(device.dev.name));
				device.dev.id = device.setup.id;
				device.dev.ff_effects_max = device.setup.ff_effects_max;
				int length = write(device.file, &device.dev, sizeof(device.dev));
				if (length < (int)sizeof(device.dev)) {
					std::string message = appendError("Fallback device setup failed ", length);
					throw std::runtime_error(message);
				}
			} else {
				std::string message = appendError("Failed to setup device ", errno);
				throw std::runtime_error(message);
			}
		}
		int errsave = errno;
		errorCode = ioctl(device.file, UI_DEV_CREATE);
		if (errorCode != 0) {
			std::string message = "Failed to create device (";
			message.append(std::to_string(errsave)).append(", ");
			message.append(std::to_string(errno)).append(")");
			throw std::runtime_error(message);
		}
		char sysfs_device_name[16];
		ioctl(device.file, UI_GET_SYSNAME(sizeof(sysfs_device_name)), sysfs_device_name);
		device.node.resize(64);
		int length = ioctl(device.file, UI_GET_SYSNAME(device.node.size()), device.node.c_str());
		if (length > 0) {
			device.node.resize(length - 1);
		} else {
			device.node.resize(0);
		}
		device.created = true;
	}

	void Backend::destroyDevice(Device& device) {
		int errorCode = 0; 
		if (device.created) {
			errorCode |= ioctl(device.file, UI_DEV_DESTROY);
		}
		if (device.opened) {
			errorCode |= close(device.file);
		}
		device.file = 0;
		device.node = "";
		device.created = false;
		device.opened = false;
		memset(&device.setup, 0, sizeof(device.setup));
		if (errorCode != 0) {
			std::string message = appendError("Device did not properly close ", errorCode);
			message.append(". Disconnected anyway.");
			throw std::runtime_error(message);
		}
	}

	void Backend::sendSyncEvent(Device& device, int report) {
		input_event event = {};
		event.type = EV_SYN;
		event.code = report;
		int length = write(device.file, &event, sizeof(event));
		if (length < 0) {
			std::string message = appendError("Failed to send sync event ", length);
			throw std::runtime_error(message);
		}
		if (length != sizeof(event)) {
			std::string message = appendError("Incomplete sond of sync event ", length);
			throw std::runtime_error(message);
		}
	}

	void Backend::activateAbsoluteEvents(Device& device) {
		int errorCode = ioctl(device.file, UI_SET_EVBIT, EV_ABS);
		if (errorCode != 0) {
			std::string message = appendError("Failed to activate absolute events ", errno);
			throw std::runtime_error(message);
		}
	}

	void Backend::activateAbsoluteAxis(Device& device, int code, input_absinfo info) {
		int errorCode = ioctl(device.file, UI_SET_ABSBIT, code);
		if (errorCode != 0) {
			std::string message = appendError("Failed to activate absolute axis ", errno);
			throw std::runtime_error(message);
		}
		if (code >= 0 && code < ABS_CNT) {
			device.dev.absmin[code] = info.minimum;
			device.dev.absmax[code] = info.maximum;
			device.dev.absfuzz[code] = info.fuzz;
			device.dev.absflat[code] = info.flat;
		}
		uinput_abs_setup setup {
			(uint16_t)code,
			info
		};
		errorCode = ioctl(device.file, UI_ABS_SETUP, &setup);
		if (errorCode != 0) {
			std::string message = appendError("Failed to setup absolute axis ", errno);
			throw std::runtime_error(message);
		}
	}

	void Backend::sendAbsoluteAxis(Device& device, int code, int value) {
		input_event event = {};
		event.type = EV_ABS;
		event.code = code;
		event.value = value;
		int length = write(device.file, &event, sizeof(event));
		if (length < 0) {
			std::string message = appendError("Failed to send absolute axis ", length);
			throw std::runtime_error(message);
		}
		if (length != sizeof(event)) {
			std::string message = appendError("Incomplete send of absolute axis ", length);
			throw std::runtime_error(message);
		}
	}

	void Backend::activateKeyEvents(Device& device) {
		int errorCode = ioctl(device.file, UI_SET_EVBIT, EV_KEY);
		if (errorCode != 0) {
			std::string message = appendError("Failed to activate key events ", errno);
			throw std::runtime_error(message);
		}
	}

	void Backend::activateKey(Device& device, int code) {
		int errorCode = ioctl(device.file, UI_SET_KEYBIT, code);
		if (errorCode != 0) {
			std::string message = appendError("Failed to activate key ", errno);
			throw std::runtime_error(message);
		}
	}

	void Backend::sendKeyEvent(Device& device, int code, bool pressed) {
		input_event event = {};
		event.type = EV_KEY;
		event.code = code;
		event.value = pressed ? 1 : 0;
		int length = write(device.file, &event, sizeof(event));
		if (length < 0) {
			std::string message = appendError("Failed to send key ", length);
			throw std::runtime_error(message);
		} 
		if (length != sizeof(event)) {
			std::string message = appendError("Incomplete send of key event", length);
			throw std::runtime_error(message);
		}
	}

	void Backend::sendFFBGainEvent(Device& device, int gain) {
		input_event event = {};
		event.type = EV_FF;
		event.code = FF_GAIN;
		event.value = 0xFFFF*std::min(100, std::max(0, gain))/100;
		int length = write(device.file, &event, sizeof(event));
		if (length < 0) {
			std::string message = appendError("Failed to send FFB gain ", length);
			throw std::runtime_error(message);
		}
		if (length != sizeof(event)) {
			std::string message = appendError("Incomplete send of FFB gain ", length);
			throw std::runtime_error(message);
		}
	}
	
	void Backend::sendFFBAutoCenterEvent(Device& device, int autoCenter) {
		input_event event = {};
		event.type = EV_FF;
		event.code = FF_AUTOCENTER;
		event.value = 0xFFFF*std::min(100, std::max(0, autoCenter))/100;
		int length = write(device.file, &event, sizeof(event));
		if (length < 0) {
			std::string message = appendError("Failed to send FFB autocenter ", length);
			throw std::runtime_error(message);
		}
		if (length != sizeof(event)) {
			std::string message = appendError("Incomplete send of FFB autocenter ", length);
			throw std::runtime_error(message);
		}
	}

	void Backend::sendFFBPlaybackEvent(Device& device, int id, int times) {
		input_event event = {};
		event.type = EV_FF;
		event.code = id;
		event.value = times;
		int length = write(device.file, &event, sizeof(event));
		if (length < 0) {
			std::string message = appendError("Failed to send FFB playback ", length);
			throw std::runtime_error(message);
		}
		if (length != sizeof(event)) {
			std::string message = appendError("Incomplete send of FFB playback ", length);
			throw std::runtime_error(message);
		}
	}

	void Backend::sendFFBUploadEvent(Device& device, uinput_ff_upload& upload) {
		int errorCode = ioctl(device.file, EVIOCSFF, &upload.effect);
		if (errorCode != 0) {
			std::string message = appendError("Failed to send FFB upload ", errno);
			throw std::runtime_error(message);
		}
	}

	void Backend::sendFFBEraseEvent(Device& device, uinput_ff_erase& erase) {
		int errorCode = ioctl(device.file, EVIOCRMFF, erase.effect_id);
		if (errorCode != 0) {
			std::string message = appendError("Failed to send FFB erase ", errno);
			throw std::runtime_error(message);
		}
	}

	void Backend::activateFFBEvents(Device& device, int effectNo) {
		int errorCode = ioctl(device.file, UI_SET_EVBIT, EV_FF);
		if (errorCode != 0) {
			std::string message = appendError("Failed to activate FFB events ", errno);
			throw std::runtime_error(message);
		}
		device.setup.ff_effects_max = effectNo;
	}

	void Backend::activateFFBEffect(Device& device, int code) {
		int errorCode = ioctl(device.file, UI_SET_FFBIT, code);
		if (errorCode != 0) {
			std::string message = appendError("Failed to active FFB effect ", errno);
			throw std::runtime_error(message);
		}
	}

	bool Backend::waitForEvent(Device& device, int timeout) {
		pollfd files = { device.file, POLLIN, 0 };
		int errorCode = poll(&files, 1, timeout);
		if (errorCode == 0) {
			return false; // timed out
		}
		if (errorCode > 0) {
			return true;
		}
		if (errno == EINTR) {
			return false; // signal occurrred
		}
		std::string message = "Failed to wait for event (";
		message.append(std::to_string(errorCode)).append(", ");
		message.append(std::to_string(errno)).append(")");
		throw std::runtime_error(message);
	}
	
	uint16_t Backend::waitForEvent(Device** devices, int timeout) {
		size_t size = sizeof(uint16_t)*8;
		pollfd files[size];
		size_t length = 0;
		for (; length < size; length++) {
			if (devices[length] == nullptr) {
				break;
			}
			files[length] = { devices[length]->file, POLLIN, 0 };
		}
		int errorCode = poll(files, length, timeout);
		if (errorCode == 0) {
			return 0; // timed out
		}
		if (errorCode > 0) {
			uint16_t result = 0;
			for (uint8_t bit = 0; bit < length; bit++) {
				// in addition to the expected events there can be
				// POLLHUP, POLLERR, POLLNVAL
				// instead we assume that these do not occur
				if (files[bit].revents != 0) {
					uint16_t mask = 0x1<<bit;
					result |= mask;
				}
			}
			return result;
		}
		if (errno == EINTR) {
			return 0; // interrupted
		}
		std::string message = "Failed to wait for event (";
		message.append(std::to_string(errorCode)).append(", ");
		message.append(std::to_string(errno)).append(")");
		throw std::runtime_error(message);
	}

	int Backend::getPendingEvents(Device& device) {
		return device.pendingNo;
	}

	input_event Backend::readEvent(Device& device, bool pop) {
		if (device.pendingNo == 0) {
			int length = read(device.file, &device.pendingEvents, sizeof(device.pendingEvents));
			if (length < 0) {
				std::string message = appendError("Failed to read event ", length);
				throw std::runtime_error(message);
			}
			if (length%sizeof(input_event) != 0) {
				std::string message = appendError("Read events of invalid length ", length);
				throw std::runtime_error(message);
			}
			device.pendingNo = length/sizeof(input_event);
			device.pendingStart = 0;
		}
		if (device.pendingNo > 0) {
			int start = device.pendingStart;
			if (pop) {
				device.pendingStart += 1;
				device.pendingNo -= 1;
			}
			return device.pendingEvents[start];
		}
		// fallback to noop
		input_event event = {};
		event.type = EV_SYN;
		event.code = SYN_REPORT;
		return event;
	}

	uinput_ff_upload Backend::beginUploadFFBEffect(Device& device, input_event trigger) {
		uinput_ff_upload effect = {};
		effect.request_id = trigger.value;
		int errorCode = ioctl(device.file, UI_BEGIN_FF_UPLOAD, &effect);
		if (errorCode != 0) {
			std::string message = appendError("Failed to upload FFB effect ", errno);
			throw std::runtime_error(message);
		}
		effect.retval = 0;
		return effect;
	}

	void Backend::endUploadFFBEffect(Device& device, uinput_ff_upload& effect) {
		int errorCode = ioctl(device.file, UI_END_FF_UPLOAD, &effect);
		if (errorCode != 0) {
			std::string message = appendError("Failed to finish FFB effect upload ", errno);
			throw std::runtime_error(message);
		}
	}

	uinput_ff_erase Backend::beginEraseFFBEffect(Device& device, input_event trigger) {
		uinput_ff_erase effect = {};
		effect.request_id = trigger.value;
		int errorCode = ioctl(device.file, UI_BEGIN_FF_ERASE, &effect);
		if (errorCode != 0) {
			std::string message = appendError("Failed to erase FFB effect ", errno);
			throw std::runtime_error(message);
		}
		effect.retval = 0;
		return effect;
	}

	void Backend::endEraseFFBEffect(Device& device, uinput_ff_erase& effect) {
		int errorCode = ioctl(device.file, UI_END_FF_ERASE, &effect);
		if (errorCode != 0) {
			std::string message = appendError("Failed to finish FFB effect erase ", errno);
			throw std::runtime_error(message);
		}
	}

	Backend::Device Backend::openDevice(const std::string& path) {
		int file = open(path.c_str(), O_RDWR);
		if (file >= 0) {
			Device device = {};
			device.file = file;
			device.node = path;
			device.opened = true;
			device.pendingCapacity = sizeof(device.pendingEvents)/sizeof(device.pendingEvents[0]);
			return device;
		} else {
			std::string errors = path;
			switch (errno) {
				case EACCES:
					errors.append(" is not accessible\n");
				break;				
				case ENOENT:
					errors.append(" does not exist\n");;
				break;
				default:
					errors.append(" cannot be opened (").append(std::to_string(errno)).append(")\n");
				break;
			}
			throw std::runtime_error(errors);
		}
	}

	void Backend::completeDeviceParams(Device& device) {
		int length = ioctl(device.file, EVIOCGNAME(sizeof(device.setup.name)), &device.setup.name);
		if (length < 0) {
			std::string message = appendError("Failed to get device name ", errno);
			throw std::runtime_error(message);
		}
		int errorCode = ioctl(device.file, EVIOCGID, &device.setup.id);
		if (errorCode != 0) {
			std::string message = appendError("Failed to get device id ", errno);
			throw std::runtime_error(message);
		}
		errorCode = ioctl(device.file, EVIOCGEFFECTS, &device.setup.ff_effects_max);
		if (errorCode != 0) {
			std::string message = appendError("Failed to get concurrent effects ", errno);
			throw std::runtime_error(message);
		}
	}
	
	void Backend::grabDevice(Device& device) {
		if (device.grabbed) {
			// the device cannot be grabbed/ungrabbed multiple times
			// already done in this case
			return; 
		}
		int errorCode = ioctl(device.file, EVIOCGRAB, 1);
		if (errorCode != 0) {
			std::string message = appendError("Failed to grab device ", errno);
			throw std::runtime_error(message);
		}
		device.grabbed = true;
	}
    
	void Backend::ungrabDevice(Device& device) {
		if (!device.grabbed) {
			return;
		}
		int errorCode = ioctl(device.file, EVIOCGRAB, 0);
		if (errorCode != 0) {
			std::string message = appendError("Failed to ungrab device ", errno);
			throw std::runtime_error(message);
		}
		device.grabbed = false;
	}
	
	bool Backend::checkDeviceConnected(Device& device) {
		char buffer[20];
		/*memset(buffer, 0, sizeof(buffer));
		int length = ioctl(device.file, EVIOCGPHYS(sizeof(buffer)), buffer);
		if (length < 0) {
			return false;
		}
		*/
		memset(buffer, 0, sizeof(buffer));
		int length = ioctl(device.file, EVIOCGUNIQ(sizeof(buffer)), buffer);
		return length >= 0;
	}
	
	bool Backend::checkDeviceExists(const std::string& path) {
		int errorCode = access(path.c_str(), F_OK);
		return errorCode == 0;
	}
	
	Backend::EventBitmap Backend::getEventBitmap(Device& device) {
		EventBitmap map = {};
		int length = ioctl(device.file, EVIOCGBIT(0, sizeof(EventBitmap)), &map);
		if (length < 0) {
			std::string message = appendError("Failed to get event bitmap ", errno);
			throw std::runtime_error(message);
		}
		return map;
	}

	Backend::KeyBitmap Backend::getKeyBitmap(Device& device) {
		KeyBitmap map = {};
		int length = ioctl(device.file, EVIOCGBIT(EV_KEY, sizeof(KeyBitmap)), &map);
		if (length < 0) {
			std::string message = appendError("Failed to get key bitmap ", errno);
			throw std::runtime_error(message);
		}
		return map;
	}

	Backend::AbsoluteBitmap Backend::getAbsoluteBitmap(Device& device) {
		AbsoluteBitmap map = {};
		int length = ioctl(device.file, EVIOCGBIT(EV_ABS, sizeof(map)), &map);
		if (length < 0) {
			std::string message = appendError("Failed to get absolute bitmap ", errno);
			throw std::runtime_error(message);
		}
		return map;
	}

	Backend::FFBBitmap Backend::getFFBBitmap(Device& device) {
		FFBBitmap map = {};
		int length = ioctl(device.file, EVIOCGBIT(EV_FF, sizeof(map)), &map);
		if (length < 0) {
			std::string message = appendError("Failed to get effect bitmap ", errno);
			throw std::runtime_error(message);
		}
		return map;
	}

	input_absinfo Backend::getAbsoluteInfo(Device& device, int code) {
		input_absinfo info = {};
		int errorCode = ioctl(device.file, EVIOCGABS(code), &info);
		if (errorCode != 0) {
			std::string message = appendError("Failed to get absolute info ", errno);
			throw std::runtime_error(message);
		}
		return info;
	}
	
	template <typename T>
	bool Backend::isBitSet(T* bitmap, int code) {
		if (code < 0 || code >= (int)sizeof(T)*8) {
			return false;
		}
		uint8_t* pointer = (uint8_t*)bitmap;
		int byte = code/8;
		int bit = code - byte*8;
		uint8_t entry = pointer[byte];
		return (entry & (0x1<<bit)) != 0;
	}
	template bool Backend::isBitSet<Backend::EventBitmap>(EventBitmap* bitmap, int code);
	template bool Backend::isBitSet<Backend::KeyBitmap>(KeyBitmap* bitmap, int code);
	template bool Backend::isBitSet<Backend::AbsoluteBitmap>(AbsoluteBitmap* bitmap, int code);
	template bool Backend::isBitSet<Backend::FFBBitmap>(FFBBitmap* bitmap, int code);

	template <typename T>
	void Backend::setMapBit(T* bitmap, int code) {
		if (code < 0 || code >= (int)sizeof(T)*8) {
			return;
		}
		uint8_t* pointer = (uint8_t*)bitmap;
		int byte = code/8;
		int bit = code - byte*8;
		pointer[byte] |= 0x1<<bit;
	}
	template void Backend::setMapBit<Backend::EventBitmap>(EventBitmap* bitmap, int code);
	template void Backend::setMapBit<Backend::KeyBitmap>(KeyBitmap* bitmap, int code);
	template void Backend::setMapBit<Backend::AbsoluteBitmap>(AbsoluteBitmap* bitmap, int code);
	template void Backend::setMapBit<Backend::FFBBitmap>(FFBBitmap* bitmap, int code);
	
	std::string Backend::appendError(std::string message, int error) {
		return message.append("(").append(std::to_string(error)).append(")");
	}
}
