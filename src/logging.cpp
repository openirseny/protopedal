#include "logging.h"

namespace proto {
	
	std::ofstream Logging::logFile;
	std::ostream Logging::discard(nullptr);
	int Logging::minimumMessagePriority = 0;
	int Logging::minimumErrorPriority = 0;
	int Logging::minimumFilePriority = 0;
	std::chrono::time_point<std::chrono::steady_clock> Logging::initTime;
	
	void Logging::init(const std::string& file) {
		initTime = std::chrono::steady_clock::now();
		if (!file.empty()) {
			logFile.open(file);
			if (!logFile.is_open()) {
				std::cerr << "Failed to open log file " << file << std::endl;
			} else {
				logFile.width(3); // probably overwritten anyway
				logFile.fill('0');
			}
		}
	}
	
	void Logging::quit() {
		if (logFile.is_open()) {
			logFile.close();
		}
	}
	
	void Logging::setMinimumMessagePriority(int priority) {
		minimumMessagePriority = priority;
	}
	
	void Logging::setMinimumErrorPriority(int priority) {
		minimumErrorPriority = priority;
	}
	
	void Logging::setMinimumFilePriority(int priority) {
		minimumFilePriority = priority;
	}

	void Logging::lineMessage(int priority, const std::string& message) {
		if (priority >= minimumMessagePriority) {
			std::cout << message << std::endl;
		}
	}
	
	
	void Logging::appendMessage(int priority, const std::string& message) {
		if (priority >= minimumMessagePriority) {
			std::cout << message;
		}
	}
	
	void Logging::lineError(int priority, const std::string& message) {
		if (priority >= minimumErrorPriority) {
			std::cerr << message << std::endl;
		}
	}
	
	void Logging::appendError(int priority, const std::string& message) {
		if (priority >= minimumErrorPriority) {
			std::cerr << message;
		}
	}
	
	void Logging::lineFile(int priority, const std::string& message) {
		if (priority >= minimumFilePriority && logFile.is_open()) {
			logFile << message << std::endl;
			logFile.flush();
		}
	}

	std::ostream& Logging::appendFile(int priority, const std::string& message) {
		if (priority >= minimumFilePriority && logFile.is_open()) {
			logFile << message;
			return logFile;
		}
		return discard;
	}
	
	std::ostream& Logging::beginMessage(int priority, bool timestamp) {
		if (priority >= minimumMessagePriority) {
			if (timestamp) {
				appendTimestamp(std::cout);
				std::cout << ": ";
			}
			return std::cout;
		}
		return discard;
	}
	
	std::ostream& Logging::beginError(int priority, bool timestamp) {
		if (priority >= minimumErrorPriority) {
			if (timestamp) {
				appendTimestamp(std::cerr);
				std::cerr << ": ";
			}
			return std::cerr;
		}
		return discard;
	}
	
	std::ostream& Logging::beginFile(int priority, bool timestamp) {
		if (priority >= minimumFilePriority) {
			if (timestamp) {
				appendTimestamp(logFile);
				logFile << ": ";
			}
			return logFile;
		}
		return discard;
	}
	
	void Logging::endMessage(int priority) {
		if (priority >= minimumMessagePriority) {
			std::cout << std::endl;
		}
	}
	
	void Logging::endError(int priority) {
		if (priority >= minimumErrorPriority) {
			std::cerr << std::endl;
		}
	}
	
	void Logging::endFile(int priority) {
		if (priority >= minimumFilePriority) {
			logFile << std::endl;
			logFile.flush();
		}
	}
	
	long Logging::milliSecondsSinceInit() {
		std::chrono::time_point<std::chrono::steady_clock> now = std::chrono::steady_clock::now();
		std::chrono::milliseconds milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(now - initTime);
		return milliseconds.count();
	}
	
	void Logging::appendTimestamp(std::ostream& stream) {
		long milliseconds = milliSecondsSinceInit();
		long seconds = milliseconds/1000;
		stream << std::setw(4) << std::setfill(' ');
		stream << seconds << ".";
		stream << std::setw(4) << std::setfill('0') << std::left;
		stream << (milliseconds - seconds*1000);
		stream << std::right;
	}

}
