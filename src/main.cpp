#include <signal.h>
#include <iostream>
#include <vector>

#include "backend.h"
#include "virtual_device.h"
#include "physical_device.h"
#include "device_mapper.h"
#include "option_parser.h"
#include "logging.h"

using namespace proto;

volatile bool running = true;

void signalExit(int signal) {
	running = false;
}

void waitForDevicesInit(DeviceSetup& setup);

int main(int argc, char **args)
{
	signal(SIGINT, signalExit);
	signal(SIGTERM, signalExit);
    signal(SIGQUIT, signalExit);
	signal(SIGKILL, signalExit);
	Logging::init("");
	DeviceSetup setup;
	OptionParser parser;
	CommonOptions options = {};
	if (!parser.parse(argc, args, setup, options)) {
		return -1;
	}
	Logging::init(options.ffbLog);
	Logging::setMinimumMessagePriority(options.minimumMessagePriority);
	if (options.daemonMode) {
		waitForDevicesInit(setup);
	}
	if (!running) {
		return 0;
	}
	VirtualDevice virt;
	std::vector<PhysicalDevice> phys;
	phys.reserve(setup.devicePaths.size());
	for (size_t i = 0; i < setup.devicePaths.size(); i++) {
		phys.emplace_back(i);
	}
	DeviceMapper mapper;
	if (!mapper.initialize(setup, phys, virt)) {
		mapper.finish(setup, phys, virt);
		return -1;
	}
	bool anyConnected = true;
    
	for (int loop = 1; running; loop = (loop + 1)&0x3FF) {
		if (loop == 0) {
			anyConnected = false;
			bool anyDisconnected = false;
			for (PhysicalDevice& dev : phys) {
				int index = dev.getIndex();
				bool open = dev.isOpen();
				if (open) {
					if (dev.checkConnected()) {
						anyConnected = true;
					} else {
						std::ostream& message = Logging::beginMessage(4);
						message << "Device " << setup.devicePaths[index] << " does no longer exist";
						if (options.daemonMode) {
							message << std::endl;
							mapper.finishPhys(setup, dev);
							message << "Waiting for it to become available again";
						}
						// give the device one chance (or many if in daemon mode) to work again
						// by checking file existance and retry reinitialization below
						open = false;
						Logging::endMessage(4);
					}
				} 
				if (!open) {
					if (PhysicalDevice::checkExists(setup.devicePaths[index])) {
						if (mapper.initializePhys(setup, dev)) {
							Logging::beginMessage(4) << "Device " << setup.devicePaths[index] << " reappeared";
							Logging::endMessage(4);
						}
					} else {
						anyDisconnected = true;
					}
				}
			}
			if (anyDisconnected && !options.daemonMode) {
				running = false;
			}
		}
		if (anyConnected) {
			mapper.continueMapping(setup, phys, virt);
		} else if (running) {
			usleep(1000);
		}
	}
	mapper.finish(setup, phys, virt);
	Logging::quit();
	return 0;
}


void waitForDevicesInit(DeviceSetup& setup) {
	std::vector<bool> exists(setup.devicePaths.size(), true);
	bool missing = true;
	for (uint8_t loop = 0; missing && running; loop++) {
		if (loop == 0) {
			missing = false;
			for (size_t i = 0; i < setup.devicePaths.size(); i++) {
				if (setup.deviceRequired[i]) {
					if (PhysicalDevice::checkExists(setup.devicePaths[i])) {
						if (!exists[i]) {
							std::ostream& message = Logging::beginMessage(4);
							message << "Device " << setup.devicePaths[i] << " appeared";
							Logging::endMessage(4);
							exists[i] = true;
						}
					} else {
						missing = true;
						if (exists[i]) {
							std::ostream& message = Logging::beginMessage(4);
							message << "Waiting for " << setup.devicePaths[i] << " to become available";
							Logging::endMessage(4);
							exists[i] = false;
						}
					}
				}
			}
		}
		usleep(4000);
	}
}