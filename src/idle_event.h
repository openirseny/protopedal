#ifndef PROTO_IDLE_EVENT_H
#define PROTO_IDLE_EVENT_H

#include "device_event.h"
#include "util.h"

namespace proto {
	class IdleEvent : public DeviceEvent {
	private:
		int device;
	public:
		IdleEvent(int device);
		virtual ~IdleEvent() {};
	public:
		virtual int getType() override;
		virtual int getDevice() override;
	};
}

#endif
