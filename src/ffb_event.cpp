#include "ffb_event.h"

namespace proto {
	FFBEvent::FFBEvent(PayloadType type) : DeviceEvent(),
		type(type),
		payload({}) {}
		
	FFBEvent::FFBEvent(uinput_ff_upload& upload) : FFBEvent(PayloadType::UPLOAD) {
		std::memcpy(payload.payload, &upload, sizeof(upload));
	}
	
	FFBEvent::FFBEvent(uinput_ff_erase& erase) : FFBEvent(PayloadType::ERASE) {
		std::memcpy(payload.payload, &erase, sizeof(erase));
	}
	
	FFBEvent::FFBEvent(int value, bool gain) : FFBEvent(gain ? PayloadType::GAIN : PayloadType::AUTOCENTER) {
		std::memcpy(payload.payload, &value, sizeof(value));
	}
	
	FFBEvent::FFBEvent(int id, int timesToPlay) : FFBEvent(PayloadType::PLAYBACK) {
		int data[2] = { timesToPlay, id };
		std::memcpy(payload.payload, data, sizeof(data));
	}

	FFBEvent::~FFBEvent()
	{
	}

	int FFBEvent::getType() {
		return EV_FF;
	}
	
	int FFBEvent::getDevice() {
		return 0;
	}
	
	int FFBEvent::getID() {
		switch (type) {
			case PayloadType::UPLOAD:
				return ((uinput_ff_upload*)payload.payload)->effect.id;
			case PayloadType::ERASE:
				return ((uinput_ff_erase*)payload.payload)->effect_id;
			case PayloadType::PLAYBACK:
				return ((int*)payload.payload)[1];
			default:
				return -1;
		}
	}
	
	void FFBEvent::setID(int id) {
		switch (type) {
			case PayloadType::UPLOAD:
				((uinput_ff_upload*)payload.payload)->effect.id = id;
				break;
			case PayloadType::ERASE:
				((uinput_ff_erase*)payload.payload)->effect_id = id;
				break;
			case PayloadType::PLAYBACK:
				((int*)payload.payload)[1] = id;
			default:
				break;
		}
	}
	
	void FFBEvent::setResult(int errorCode) {
		switch (type) {
			case PayloadType::UPLOAD:
				((uinput_ff_upload*)payload.payload)->retval = errorCode;
				break;
			case PayloadType::ERASE:
				((uinput_ff_erase*)payload.payload)->retval = errorCode;
				break;
			default:
				break;
		}
	}
	
	FFBEvent::PayloadType FFBEvent::getPayloadType() {
		return type;
	}
	
	int* FFBEvent::getGainPayload() {
		if (type != PayloadType::GAIN) {
			return nullptr;
		}
		return (int*)payload.payload;
	}
	
	int* FFBEvent::getAutoCenterPayload() {
		if (type != PayloadType::AUTOCENTER) {
			return nullptr;
		}
		return (int*)payload.payload;
	}
	
	uinput_ff_upload* FFBEvent::getUploadPayload() {
		if (type != PayloadType::UPLOAD) {
			return nullptr;
		}
		return (uinput_ff_upload*)payload.payload;
	}
	
	uinput_ff_erase* FFBEvent::getErasePayload() {
		if (type != PayloadType::ERASE) {
			return nullptr;
		}
		return (uinput_ff_erase*)payload.payload;
	}
	
	int* FFBEvent::getPlaybackPayload() {
		if (type != PayloadType::PLAYBACK) {
			return nullptr;
		}
		return (int*)payload.payload;
	}
}


