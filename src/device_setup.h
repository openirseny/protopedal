#ifndef PROTO_DEVICE_SETUP_H
#define PROTO_DEVICE_SETUP_H

#include <unordered_map>
#include <string>
#include <map>
#include <vector>
#include <algorithm>
#include <cmath>
#include <climits>
#include <cstdint>

#include <linux/input.h>

#include "util.h"

namespace proto {
	class CurveDescription;
	class EventPlugin;
	
	class DeviceSetup {
	public:
		struct EventMap {
			int type;
			int code;
			// TODO this is phys  min/max and sould be source min/max, also add dst and virt min/max
			int minimum;
			int maximum;
			bool invert;
			CurveDescription* curve;
			EventPlugin* plugin;
			std::string args;
		};
		struct AxisSetup {
			int minimum;
			int maximum;
		};
		struct ButtonSetup {
			bool toggle;
		};
		union CapabilitySetup {
			AxisSetup axis;
			ButtonSetup button;
		};
		struct EventSource {
			int device;
			int type;
			int code;
		};
		struct EventSourceHash {
			size_t operator()(const EventSource& key) const {
				return (key.device<<16) ^ (key.type<<8) ^ key.code;
			}
		};
		struct EventSourceEqual {
			bool operator()(const EventSource& key1, const EventSource& key2) const {
				return key1.device == key2.device && key1.type == key2.type && key1.code == key2.code;
			}
		};
		struct CapabilityKey {
			int type;
			int code;
		};
		struct CapabilityKeyHash {
			size_t operator()(const CapabilityKey& key) const {
				return (key.type<<8) | key.code;
			}
		};
		struct CapabilityKeyEqual {
			bool operator()(const CapabilityKey& key1, const CapabilityKey key2) const {
				return key1.type == key2.type && key1.code == key2.code;
			}
		};
	public:
		unsigned short vendor;
		unsigned short product;
		std::string name;
		std::vector<std::string> devicePaths;
		std::vector<bool> deviceRequired;
		std::unordered_map<CapabilityKey, CapabilitySetup, CapabilityKeyHash, CapabilityKeyEqual> capabilitySetup;
		std::unordered_multimap<EventSource, EventMap, EventSourceHash, EventSourceEqual> capabilityMap;
		unsigned int minAxisNo;
		unsigned int minButtonNo;
		unsigned int maxAxisNo;
		unsigned int maxButtonNo;
		int fixedGain;
		int fixedAutoCenter;
		bool reportAxisLimits;
		bool reportButtonLimits;
        bool exclusiveAccess;
		int ffbDevice;
	public:
		DeviceSetup();
	public:
		static CapabilityKey toCapability(EventSource& source);
		static CapabilityKey toCapability(EventMap& map);
		static void completeRanges(input_absinfo& sourceInfo, EventMap& targetInfo, AxisSetup& targetSetup, AxisSetup& fallback);
		static int mapToRange(int value, int inmin, int inmax, int outmin, int outmax, bool invert);
		static AxisSetup toAxisSetup(input_absinfo& info);
		static bool addDevicePath(DeviceSetup&, const std::string& path, bool required);
		static void setName(DeviceSetup& setup, const std::string& name);
		static void setVendor(DeviceSetup& setup, int vendor);
		static void setProduct(DeviceSetup& setup, int product);
		static void enableCapability(DeviceSetup& setup, CapabilityKey& key);
		static void mapCapability(DeviceSetup& setup, EventSource& source, EventMap& mapInfo);
		static void setMinAxisNo(DeviceSetup& setup, unsigned int axes);
		static void setMinButtonNo(DeviceSetup& setup, unsigned int buttons);
		static void setMaxAxisNo(DeviceSetup& setup, unsigned int axes);
		static void setMaxButtonNo(DeviceSetup& setup, unsigned int buttons);
		static void ignoreAdditionalButtons(DeviceSetup& setup);
		static void setFixedGain(DeviceSetup& setup, int gain);
		static void setFixedAutoCenter(DeviceSetup& setup, int autoCenter);
		static void reportCapabilityLimits(DeviceSetup& setup);
		static void enableExclusiveAccess(DeviceSetup& setup);
		static void setFFBDevice(DeviceSetup& setup, int device);
	};
}



#endif
