#include "option_parser.h"
#include "curve_description.h"
#include "event_plugin.h"

namespace proto {
	void OptionParser::displayUsage() {
		std::cout << " [--help] [--name <name>] [--vendor <vendor>] [--product <product>]" << std::endl;
		std::cout << " [--axis <axis> [--source <axis>] [--source-button <button>] [--min <min> --max <max>] [--invert] [--curve <file>] [--plugin <./file> --args <args>]]*" << std::endl;
		std::cout << " [--button <button> [--source <button>] [--source-axis <axis>] [--min <min> --max <max>] [--invert] [--curve <file>] [--plugin <./file> --args <args>]]*" << std::endl;
		std::cout << " [--axes <number>] [--buttons <number>] [--no-auto-axes] [--no-auto-buttons] [--max-auto-axes <number>] [--max-auto-axes <number>]" << std::endl;
		std::cout << " [--gain <percent>] [--autocenter <percent>] [--ffb-device <index>] [--ffb-log <file>]" << std::endl;
		std::cout << " [--grab] [--report-limits] [--verbose]  <device>+" << std::endl << std::endl;

		std::cout << " -h, --help                   Print this text" << std::endl;
		std::cout << " <device>                     Physical device to mimic" << std::endl << std::endl;

		std::cout << "Basic device information:" << std::endl;
		std::cout << " -n, --name <name>            Virtual device name" << std::endl;
		std::cout << " -v, --vendor <hex>           Virtual device vendor" << std::endl;
		std::cout << " -p, --product <hex>          Virtual device product" << std::endl << std::endl;
		
		std::cout << "Button and axis customization:" << std::endl;
		std::cout << " -b, --button <button>        Virtual device button to customize" << std::endl;
		std::cout << " -a, --axis <axis>            Virtual device axis to customize" << std::endl;
		std::cout << " -s, --source <button>/<axis> Physical device button or axis to read from" << std::endl;
		std::cout << " --source-axis <axis>         Physical device axis to read from" << std::endl;
		std::cout << " --source-button <button>     Physical device button to read from" << std::endl;
		std::cout << " -m, --min <number>           Lower bound" << std::endl;
		std::cout << " -M, --max <number>           Upper bound" << std::endl;
		std::cout << " -i, --invert                 Invert output" << std::endl;
		std::cout << " --curve <file>               Axis curve CSV file" << std::endl;
		std::cout << " --plugin <./file>            Event plugin library" << std::endl;
		std::cout << " --args <args>                Event plugin initialization arguments" << std::endl;
		std::cout << " --buttons <number>           Minimum number of buttons advertized by the virtual device" << std::endl;
		std::cout << " --axes <number>              Minimum number of axes advertized by the virtual device" << std::endl;
		std::cout << " --no-auto-buttons            Do not create mappings for non-specified buttons" << std::endl;
		std::cout << " --no-auto-axes               Do not create mappings for non-specified axes" << std::endl;
		std::cout << " --max-auto-buttons           Maximum number of buttons for automatic mappings" << std::endl;
		std::cout << " --max-auto-axes              Maximum number of axes for automatic mappings"  << std::endl << std::endl;
		
		std::cout << "Force feedback settings:" << std::endl;
		std::cout << " --gain <percent>             Fixed force feedback gain" << std::endl;        
		std::cout << " --autocenter <percent>       Fixed auto centering" << std::endl;
		std::cout << " --ffb-device <index>         Index of the device to advertize FFB capabilites from. Defaults to 0." << std::endl;
		std::cout << " --ffb-log <file>             Log file for force feedback effects" << std::endl << std::endl;
		
		std::cout << "Other options:" << std::endl;
		std::cout << " --list-buttons               List recognized buttons and exit" << std::endl;
		std::cout << " --list-axes                  List recognized axes and exit" << std::endl;
		std::cout << " --grab                       Access the physical device exclusively, other programs do not receive events" << std::endl;
		std::cout << " --report-limits              Report physical axis values and button clicks that appeared during testing" << std::endl; 
		std::cout << " --verbose                    Enable verbose output" << std::endl;
		std::cout << " --daemon                     Continue operation when device gets lost" << std::endl << std::endl;
		

		std::cout << "Devices can be referenced in <axis> and <button> by prepending the device index leading to the syntax" << std::endl;
		std::cout << "<index>:<capability> when using multiple physical devices" << std::endl;
		std::cout << "Unspecified options are read from the device to mimic." << std::endl;
		std::cout << "By default existing axes and buttons are mapped to their physical counterparts." << std::endl;
		std::cout << "Supported axes include X, Y, Z, RX, RY, RZ, THROTTLE, RUDDER, WHEEL, GAS and BRAKE among others." << std::endl;
		std::cout << "Supported buttons include A, B, C, X, Y, Z, L1, R1, L2, R2, SELECT, START, MODE, LEFT-STICK, RIGHT-STICK, WHEEL, GEAR-DOWN and GEAR-UP among others." << std::endl;
	}
	
	void OptionParser::displayUsageError(const std::string& program, const std::string& param, const std::string& arg, const std::string& error) {
		std::cerr << program << ": option '" << param;
		if (arg.length() > 0) {
			std::cerr << " " << arg << "': " << error << std::endl;
		} else {
			std::cerr << "': " << error << std::endl;
		}
	}
	
	void OptionParser::displayButtons() {
		std::cout << std::hex << std::uppercase;
		for (int b = BTN_MIN; b < BTN_MAX; b++) {
			std::string name = Util::getButtonName(b);
			if (name.size() > 0 && name[0] != '0') {
				std::cout << name;
				for (int c = name.size(); c < 11 ; c++) {
					std::cout << ' ';
				}
				std::cout << " (0x" << b << ")" << std::endl;
			}
		}
		std::cout << std::dec << std::nouppercase;
	}
	
	void OptionParser::displayAxes() {
		std::cout << std::hex << std::uppercase;
		for (int a = ABS_MIN; a < ABS_MAX; a++) {
			std::string name = Util::getAxisName(a);
			if (name.size() > 0 && name[0] != '0') {
				std::cout << name;
				for (int c = name.size(); c < 11; c++) {
					std::cout << ' ';
				}
				std::cout << " (0x" << a << ")" << std::endl;
			}
		}
		std::cout << std::dec << std::nouppercase;
	}
	
	bool OptionParser::parse(int argc, char** args, DeviceSetup& setup, CommonOptions& options) {
		options.minimumMessagePriority = 4;
		const option longParams[] = {
			{ "help", no_argument, NULL, 'h' },
			{ "name", required_argument, NULL, 'n' },
			{ "vendor", required_argument, NULL, 'v' },
			{ "product", required_argument, NULL, 'p' },
			{ "axis", required_argument, NULL, 'a' },
			{ "button", required_argument, NULL, 'b' },
			{ "min", required_argument, NULL, 'm' },
			{ "max", required_argument, NULL, 'M' },
			{ "source", required_argument, NULL, 's' },
			{ "source-axis", required_argument, NULL, 'x' },
			{ "source-button", required_argument, NULL, 'u' },
			{ "invert", no_argument, NULL, 'i' },
			{ "curve", required_argument, NULL, 'c' },
			{ "plugin", required_argument, NULL, 'P' },
			{ "args", required_argument, NULL, 'R' },
			{ "axes", required_argument, NULL, 'A' },
			{ "buttons", required_argument, NULL, 'B' },
			{ "no-auto-axes", no_argument, NULL, 'X' }, // for backwards compatibility
			{ "no-auto-buttons", no_argument, NULL, 'U' }, // max-auto-... serves same functionality
			{ "max-auto-axes", required_argument, NULL, 'X' },
			{ "max-auto-buttons", required_argument, NULL, 'U' },
			{ "gain", required_argument, NULL, 'g' },
			{ "autocenter", required_argument, NULL, 'C' },
			{ "list-buttons", no_argument, NULL, '.' },
			{ "list-axes", no_argument, NULL, '|' },
			{ "grab", no_argument, NULL, 'G' },
			{ "report-limits", no_argument, NULL, 'T' },
			{ "verbose", no_argument, NULL, 'V' },
			{ "ffb-device", required_argument, NULL, 'F' },
			{ "ffb-log", required_argument, NULL, 'L' },
			{ "daemon", no_argument, NULL, 'D' },
			{ NULL, 0, NULL, 0 }
		};
		const char* shortParams = "hn:v:p:a:b:m:M:s:x:u:iA:B:X:U:TVL:g:C:G";
		ErrorCode errorCode = ErrorCode::OK;
		const char* paramName = nullptr;
		const char* argument = nullptr;
		std::string errorMessage;
		DeviceSetup::EventMap mapping;
		clearMapping(mapping);
		int axis = ABS_INVALID;
		int button = BTN_INVALID;
		int device = -1;
		int iArg = -1;
		int paramShort = 0;
		// named arguments
		while (errorCode == ErrorCode::OK) {
			iArg = -1;
			paramShort = getopt_long(argc, args, shortParams, longParams, &iArg);
			argument = optarg;
			if (paramShort < 0) { // reached end
				break;
			} else if (iArg > -1) { // exists as long option
				paramName = longParams[iArg].name;
				paramShort = longParams[iArg].val;
			} else { // only shorthand available
				paramName = (const char*)&paramShort; // works on LE
			}
			switch (paramShort) {
				case '?':
				case 'h':
				case ':':
					errorCode = ErrorCode::HELP;
					break;
				case 'n':
					DeviceSetup::setName(setup, std::string(argument));
					break;
				case 'v': {
					int vendor = 0;
					if (sscanf(argument, "%4x", &vendor) == 1) {
						DeviceSetup::setVendor(setup, vendor);
					} else {
						errorCode = ErrorCode::EXPECTED_VENDOR_PRODUCT_ID;
					}
					break;
				}
				case 'p': {
					int product = 0;
					if (sscanf(argument, "%4x", &product) == 1) {
						DeviceSetup::setProduct(setup, product);
					} else {
						errorCode = ErrorCode::EXPECTED_VENDOR_PRODUCT_ID;
					}
					break;
				}
				case 'A': {
					unsigned int axes = 0;
					if (sscanf(argument, "%u", &axes) == 1) {
						DeviceSetup::setMinAxisNo(setup, axes);
					} else {
						errorCode = ErrorCode::EXPECTED_POSITIVE_NUMBER;
					}
					break;
				}
				case 'B': {
					unsigned int buttons = 0;
					if (sscanf(argument, "%u", &buttons) == 1) {
						DeviceSetup::setMinButtonNo(setup, buttons);
					} else {
						errorCode = ErrorCode::EXPECTED_POSITIVE_NUMBER;
					}
					break;
				}
				case 'X': {
					if (argument != nullptr) {
						unsigned int axes = 0;
						if (sscanf(argument, "%u", &axes) == 1) {
							DeviceSetup::setMaxAxisNo(setup, axes);
						} else {
							errorCode = ErrorCode::EXPECTED_POSITIVE_NUMBER;
						}
					} else {
						DeviceSetup::setMaxAxisNo(setup, 0);
					}
					break;
				}
				case 'U': {
					if (argument != nullptr) {
						unsigned int buttons = 0;
						if (sscanf(argument, "%u", &buttons) == 1) {
							DeviceSetup::setMaxButtonNo(setup, buttons);
						} else {
							errorCode = ErrorCode::EXPECTED_POSITIVE_NUMBER;
						}
					} else {
						DeviceSetup::setMaxButtonNo(setup, 0);
					}
					break;
				}
				case 'V':
					options.minimumMessagePriority = 0;
					break;
				case 'F': {
					int device = 0;
					if (sscanf(argument, "%i", &device) == 1) {
						DeviceSetup::setFFBDevice(setup, device);
					} else {
						errorCode = ErrorCode::EXPECTED_POSITIVE_NUMBER;
					}
					break;
				}
				case 'L':
					options.ffbLog = std::string(argument);
					break;
				case 'T':
					DeviceSetup::reportCapabilityLimits(setup);
					break;
				case 'a': {
					finalizeMapping(setup, mapping, device, axis, button);
					if (Util::getAxisFromName(argument, axis)) {
						beginAxisMapping(mapping, axis);
					} else {
						errorCode = ErrorCode::AXIS_NOT_SUPPORTED;
					}
					break;
				}
				case 'b': {
					finalizeMapping(setup, mapping, device, axis, button);
					if (Util::getButtonFromName(argument, button)) {
						beginButtonMapping(mapping, button);
					} else {
						errorCode = ErrorCode::BUTTON_NOT_SUPPORTED;
					}
					break;
				}
				case 's': {
					std::string name;
					if (!Util::extractDeviceFromCapability(argument, device, name)) {
						errorCode = ErrorCode::CAPABILITY_ILLEGAL_FORMAT;
						break;
					}
					if (mapping.type == EV_ABS) {
						button = BTN_INVALID;
						if (!Util::getAxisFromName(name, axis)) {
							errorCode = ErrorCode::AXIS_NOT_SUPPORTED;
						}
					} else if (mapping.type == EV_KEY) {
						axis = ABS_INVALID;
						if (!Util::getButtonFromName(name, button)) {
							errorCode = ErrorCode::BUTTON_NOT_SUPPORTED;
						}
					} else {
						errorCode = ErrorCode::CAPABILITY_NOT_SPECIFIED;
					}
					break;
				}
				case 'x': {
					std::string name;
					if (!Util::extractDeviceFromCapability(argument, device, name)) {
						errorCode = ErrorCode::CAPABILITY_ILLEGAL_FORMAT;
						break;
					}
					if (!Util::getAxisFromName(name, axis)) {
						errorCode = ErrorCode::AXIS_NOT_SUPPORTED;
					}
					if (mapping.type == 0) {
						errorCode = ErrorCode::CAPABILITY_NOT_SPECIFIED;
					}
					button = BTN_INVALID;
					break;
				}
				case 'u': {
					std::string name;
					if (!Util::extractDeviceFromCapability(argument, device, name)) {
						errorCode = ErrorCode::CAPABILITY_ILLEGAL_FORMAT;
						break;
					}
					if (!Util::getButtonFromName(name, button)) {
						errorCode = ErrorCode::BUTTON_PARAM_NOT_SUPPORTED;
					}
					if (mapping.type == 0) {
						errorCode = ErrorCode::CAPABILITY_NOT_SPECIFIED;
					}
					axis = ABS_INVALID;
					break;
				}
				case 'm': {
					if (sscanf(argument, "%i", &mapping.minimum) != 1) {
						errorCode = ErrorCode::EXPECTED_NUMBER;
					}
					if (mapping.type == 0) {
						errorCode = ErrorCode::CAPABILITY_NOT_SPECIFIED;
					}
					break;
				}
				
				case 'M': {
					if (sscanf(argument, "%i", &mapping.maximum) != 1) {
						errorCode = ErrorCode::EXPECTED_NUMBER;
					} 
					if (mapping.type == 0) {
						errorCode = ErrorCode::CAPABILITY_NOT_SPECIFIED;
					}
					break;
				}
				case 'i':
					mapping.invert = true;
					if (mapping.type == 0) {
						errorCode = ErrorCode::CAPABILITY_NOT_SPECIFIED;
					}
					break;
				case 'c':
					if (mapping.type != 0) {
						CurveDescription* curve = new CurveDescription();
						try {
							curve->loadCSV(argument);
							mapping.curve = curve;
						} catch (std::exception& e) {
							delete curve;
							errorCode = ErrorCode::CURVE_INVALID;
							errorMessage = e.what();
						}
					} else {
						errorCode = ErrorCode::CAPABILITY_NOT_SPECIFIED;
					}
					break;
				case 'P':
					if (mapping.type != 0) {
						EventPlugin* plugin = new EventPlugin();
						try {
							plugin->loadSO(argument);
							mapping.plugin = plugin;
						} catch (std::exception& e) {
							delete plugin;
							errorCode = ErrorCode::PLUGIN_INVALID;
							errorMessage = e.what();
						}
					} else {
						errorCode = ErrorCode::CAPABILITY_NOT_SPECIFIED;
					}
					break;
				case 'R':
					if (mapping.type != 0) {
						mapping.args = std::string(argument);
					} else {
						errorCode = ErrorCode::CAPABILITY_NOT_SPECIFIED;
					}
					break;
				case 'C': {
					int autocenter;
					if (sscanf(argument, "%i", &autocenter) == 1) {
						DeviceSetup::setFixedAutoCenter(setup, autocenter);
					} else {
						errorCode = ErrorCode::EXPECTED_NUMBER;
					}
					break;
				}
				case 'g': {
					int gain = 100;
					if (sscanf(argument, "%i", &gain) == 1) {
						DeviceSetup::setFixedGain(setup, gain);
					} else {
						errorCode = ErrorCode::EXPECTED_NUMBER;
					}
					break;
				}
				case 'G':
					DeviceSetup::enableExclusiveAccess(setup);
					break;
				case 'D':
					options.daemonMode = true;
					break;
				case '|':
					displayAxes();
					return false;
				case '.':
					displayButtons();
					return false;
				default:
					errorCode = ErrorCode::UNRECOGNIZED_INPUT;
					break;
			}
			
		}
		// get device
		if (errorCode == ErrorCode::OK) {
			finalizeMapping(setup, mapping, device, axis, button);
			
			if (optind < argc) {
				for (; optind < argc; optind++) {
					if (!DeviceSetup::addDevicePath(setup, std::string(args[optind]), true)) {
						break;
					}
				}
			} else {
				paramName = "<device>";
				errorCode = ErrorCode::DEVICE_NOT_SPECIFIED;
			}
			
			if (optind < argc) {
				paramName = "<device>";
				errorCode = ErrorCode::SUPERFLUOUS_INPUT;
			}
		}
		// post process
		if (setup.ffbDevice >= 0 && (size_t)setup.ffbDevice > setup.devicePaths.size()) {
			DeviceSetup::setFFBDevice(setup, -1);
		}
		if (errorCode != ErrorCode::OK) {
			switch (errorCode) {
				case ErrorCode::HELP:
					break; // only display usage
				case ErrorCode::INTERNAL_ERROR:
					displayUsageError(args[0], paramName, "", "Internal error occurred");
					break;
				case ErrorCode::INVALID_INPUT:
					displayUsageError(args[0], paramName, "", "Invalid input");
					break;
				case ErrorCode::UNRECOGNIZED_INPUT:
					displayUsageError(args[0], paramName, "", "Input not recognized");
					break;
				case ErrorCode::SUPERFLUOUS_INPUT:
					displayUsageError(args[0], paramName, "", "Superfluous input");
					break;
				case ErrorCode::DEVICE_NOT_SPECIFIED:
					displayUsageError(args[0], paramName, "", "Device not specified");
					break;
				case ErrorCode::EXPECTED_NUMBER:
					displayUsageError(args[0], paramName, argument, "Expected an integer");
					break;
				case ErrorCode::EXPECTED_POSITIVE_NUMBER:
					displayUsageError(args[0], paramName, argument, "Expected a positive integer");
					break;
				case ErrorCode::EXPECTED_VENDOR_PRODUCT_ID:
					displayUsageError(args[0], paramName, argument, "Expected a 4-digit hexadecimal number");
					break;
				case ErrorCode::CAPABILITY_NOT_SPECIFIED:
					displayUsageError(args[0], paramName, argument, "Missing --axis or --button");
					break;
				case ErrorCode::CAPABILITY_ILLEGAL_FORMAT:
					displayUsageError(args[0], paramName, argument, "Invalid syntax");
					break;
				case ErrorCode::AXIS_NOT_SUPPORTED:
				case ErrorCode::BUTTON_NOT_SUPPORTED:
					displayUsageError(args[0], paramName, argument, "Not supported");
					break;
				case ErrorCode::AXIS_PARAM_NOT_SUPPORTED:
					displayUsageError(args[0], paramName, "", "Not supported for --axis");
					break;
				case ErrorCode::BUTTON_PARAM_NOT_SUPPORTED:
					displayUsageError(args[0], paramName, "", "Not supported for --button");
					break;
				case ErrorCode::CURVE_INVALID:
					displayUsageError(args[0], paramName, argument, errorMessage);
					break;
				case ErrorCode::PLUGIN_INVALID:
					displayUsageError(args[0], paramName, argument, errorMessage);
					break;
				default:
					break; // does not appear
			}
			std::cout << std::endl;
			displayUsage();
			return false;
		}
		
		return true;
	}
	
	void OptionParser::clearMapping(DeviceSetup::EventMap& mapping) {
		mapping = {};
		mapping.minimum = INT_MIN;
		mapping.maximum = INT_MIN;
	}
	
	void OptionParser::beginAxisMapping(DeviceSetup::EventMap& mapping, int axis) {
		clearMapping(mapping);
		mapping.type = EV_ABS;
		mapping.code = axis;
	}
	
	void OptionParser::beginButtonMapping(DeviceSetup::EventMap& mapping, int button) {
		clearMapping(mapping);
		mapping.type = EV_KEY;
		mapping.code = button;
		
	}
	
	void OptionParser::finalizeMapping(DeviceSetup& setup, DeviceSetup::EventMap& mapping, int& device, int& axis, int& button) {
		if (button != BTN_INVALID) {
			DeviceSetup::EventSource source = { std::max(0, device), EV_KEY, button };
			DeviceSetup::mapCapability(setup, source, mapping);
			button = BTN_INVALID;
		}
		if (axis != ABS_INVALID) {
			DeviceSetup::EventSource source = { std::max(0, device), EV_ABS, axis };
			DeviceSetup::mapCapability(setup, source, mapping);
			axis = ABS_INVALID;
		}
		device = -1;
		clearMapping(mapping);
	}
	
	
}
