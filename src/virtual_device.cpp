
#include <iostream>
#include "virtual_device.h"
#include "button_event.h"
#include "axis_event.h"
#include "ffb_event.h"
#include "sync_event.h"

namespace proto {
	
	VirtualDevice::VirtualDevice() :
		device(),
		buttons(),
		axes(),
		effects(),
		upload(),
		absEnabled(false),
		keyEnabled(false),
		ffbEnabled(0),
		setupState(0) {
	}

	VirtualDevice::~VirtualDevice() {
		close();
	}

	int VirtualDevice::collectEvents() {
		if (setupState != 2) {
			return 0;
		}
		try {
			if (Backend::waitForEvent(device, 0)) {
				Backend::readEvent(device, false);
			}
		} catch (std::exception& e) {
			return 0;
		}
		return Backend::getPendingEvents(device);
	}

	DeviceEvent* VirtualDevice::makeEvent(input_event& event) {
		switch (event.type) {
		case EV_FF:
			switch (event.code) {
				case FF_GAIN:
					return new FFBEvent(100*event.value/0xFFFF, true);
				case FF_AUTOCENTER:
					return new FFBEvent(100*event.value/0xFFFF, false);
				default:
					return new FFBEvent(event.code, event.value);
			}
			return nullptr;
		case EV_UINPUT:
			switch (event.code) {
				case UI_FF_UPLOAD: {
					uinput_ff_upload upload = Backend::beginUploadFFBEffect(device, event);
					return new FFBEvent(upload);
				}
				case UI_FF_ERASE: {
					uinput_ff_erase erase = Backend::beginEraseFFBEffect(device, event);
					return new FFBEvent(erase);
				}
				default:
					Logging::beginError(2) << "Unexpected uinput event code " << event.code << std::endl;
					return nullptr;
			}
		case EV_MSC:
			break;
		default:
			Logging::beginError(2) << "Unexpected event type " << event.type << std::endl;
			break;
		}
		return nullptr;
	}

	DeviceEvent* VirtualDevice::nextEvent() {
		if (setupState != 2) {
			return nullptr;
		}
		if (Backend::getPendingEvents(device) == 0) {
			return nullptr;
		}
		try {
			input_event source = Backend::readEvent(device, true);
			return makeEvent(source);
		} catch (std::exception& e) {
			return nullptr;
		}
	}

	bool VirtualDevice::feedbackEvent(DeviceEvent* event) {
		if (setupState != 2) {
			return false;
		}
		bool result = true;
		if (FFBEvent* ffb = dynamic_cast<FFBEvent*>(event)) {
			switch (ffb->getPayloadType()) {
				case FFBEvent::PayloadType::UPLOAD: {
					uinput_ff_upload& upload = *ffb->getUploadPayload();
					try {
						Backend::endUploadFFBEffect(device, upload);
					} catch (std::exception& e) {
						Logging::beginFile(4) << "Feedback of effect upload " << upload.effect.id;
						Logging::appendFile(4, " (") << Util::getEffectTypeName(upload.effect.type) << ")";
						Logging::appendFile(4, " failed: ") << e.what() << std::endl;
						result = false;
					}
					break;
				}
				case FFBEvent::PayloadType::ERASE: {
					uinput_ff_erase& erase = *ffb->getErasePayload();
					try {
						Backend::endEraseFFBEffect(device, erase);
					} catch (std::exception& e) {
						Logging::beginFile(4) << "Feedback of effect erase " << erase.effect_id;
						Logging::appendFile(4, " failed: ") << e.what() << std::endl;
						result = false;
					}
					break;
				}
				default:
					break;
			}
		}
		delete event;
		return result;
	}

	bool VirtualDevice::sendEvent(DeviceEvent* event) {
		if (setupState != 2) {
			return false;
		}
		bool result = true;
		switch (event->getType()) {
			case EV_SYN:
				if (SyncEvent* sync = dynamic_cast<SyncEvent*>(event)) {
					try {
						Backend::sendSyncEvent(device, sync->getReport());
					} catch (std::exception& e) {
						Logging::beginError(4) << "Failed to send to virtual: " << e.what();
						Logging::endError(4);
						result = false;
					}
				}
				break;
			case EV_KEY: {
				if (ButtonEvent* button = dynamic_cast<ButtonEvent*>(event)) {
					auto iter = buttons.find(button->getButton());
					if (iter != buttons.end()) {
						try {
							Backend::sendKeyEvent(device, button->getButton(), button->isPressed());
							iter->second = button->isPressed();
						} catch (std::exception& e) {
							Logging::beginError(4) << "Failed to send to virtual: " << e.what();
							Logging::endError(4);
							result = false;
						}
					}
				}
				break;
			}
			case EV_ABS: {
				if (AxisEvent* axis = dynamic_cast<AxisEvent*>(event)) {
					auto iter = axes.find(axis->getAxis());
					if (iter != axes.end()) {
						try {
							Backend::sendAbsoluteAxis(device, axis->getAxis(), axis->getValue());
						} catch (std::exception& e) {
							Logging::beginError(4) << "Failed to send to virtual: " << e.what();
							Logging::endError(4);
							result = false;
						}
					}
				}
				break;
			}
			default: {
				Logging::beginError(2) << "Cannot send events of type " << event->getType() << " to virtual device";
				Logging::endError(2);
				result = false;
				break;
			}
		}
		return result;
	}

	bool VirtualDevice::isOpen() {
		return setupState == 2;
	}

	bool VirtualDevice::beginOpen() {
		if (setupState != 0) {
			return false;
		}
		try {
			int root = Backend::openRoot();
			device = Backend::beginDeviceSetup(root);
		} catch (std::exception& e) {
			Logging::beginError(8) << "Failed to initialize virtual device: " << e.what();
			Logging::endError(8);
			return false;
		}
		setupState = 1;
		return true;
	}

	bool VirtualDevice::setName(const std::string& name) {
		if (setupState != 1) {
			return false;
		}
		strncpy(device.setup.name, name.c_str(), UINPUT_MAX_NAME_SIZE);
		return true;
	}

	bool VirtualDevice::setVendor(int vendor) {
		if (setupState != 1) {
			return false;
		}
		device.setup.id.vendor = vendor;
		return true;
	}

	bool VirtualDevice::setProduct(int product) {
		if (setupState != 1) {
			return false;
		}
		device.setup.id.product = product;
		return true;
	}

	bool VirtualDevice::enableAxes() {
		if (setupState != 1) {
			return false;
		}
		if (!absEnabled) {
			try {
				Backend::activateAbsoluteEvents(device);
				absEnabled = true;
			} catch (std::exception& e) {
				Logging::beginError(8) << e.what();
				Logging::endError(8);
				return false;
			}
		}
		return true;
	}

	bool VirtualDevice::getButtonState(int button) {
		auto iter = buttons.find(button);
		if (iter == buttons.end()) {
			return false;
		}
		return iter->second;
	}

	bool VirtualDevice::getAxisInfo(int axis, input_absinfo& info) {
		if (setupState < 1) {
			return false;
		}
		auto iter = axes.find(axis);
		if (iter == axes.end()) {
			return false;
		}
		info = iter->second;
		return true;
	}

	bool VirtualDevice::addAxis(int axis, input_absinfo info) {
		if (!enableAxes()) {
			return false;
		}
		try {
			Backend::activateAbsoluteAxis(device, axis, info);
			axes[axis] = info;
		} catch (std::exception& e) {
			Logging::beginError(8) << e.what();
			Logging::endError(8);
			return false;
		}
		return true;
	}

	bool VirtualDevice::enableButtons() {
		if (setupState != 1) {
			return false;
		}
		if (!keyEnabled) {
			try {
				Backend::activateKeyEvents(device);
				keyEnabled = true;
			} catch (std::exception& e) {
				Logging::beginError(8) << e.what();
				Logging::endError(8);
				return false;
			}
		}
		return true;
	}
	bool VirtualDevice::addButton(int button) {
		if (!enableButtons()) {
			return false;
		}
		try {
			Backend::activateKey(device, button);
			buttons.insert({ button, false });
		} catch (std::exception& e) {
			Logging::beginError(8) << e.what();
			Logging::endError(8);
			return false;
		}
		return true;
	}

	bool VirtualDevice::hasButton(int button) {
		if (setupState < 1) {
			return false;
		}
		return buttons.count(button) > 0;
	}

	bool VirtualDevice::enableFFB(int effectNo) {
		if (setupState != 1) {
			return false;
		}
		if (ffbEnabled == 0 || (effectNo > 1 && ffbEnabled != effectNo)) {
			try {
				Backend::activateFFBEvents(device, effectNo);
				ffbEnabled = effectNo;
			} catch (std::exception& e) {
				Logging::beginError(8) << e.what();
				Logging::endError(8);
				return false;
			}
		}
		return true;
	}

	bool VirtualDevice::addFFBEffect(int effect) {
		if (!enableFFB(1)) {
			return false;
		}
		try {
			Backend::activateFFBEffect(device, effect);
		} catch (std::exception& e) {
			Logging::beginError(8) << e.what();
			Logging::endError(8);
			return false;
		}
		return true;
	}

	bool VirtualDevice::finishOpen() {
		if (setupState != 1) {
			return false;
		}
		try {
			Backend::finishDeviceSetup(device);
		} catch (std::exception& e) {
			Logging::beginError(8) << "Failed to initialize virtual device: " << e.what();
			Logging::endError(8);
			return false;
		}
		setupState = 2;
		return true;
	}

	void VirtualDevice::close() {
		if (setupState < 1) {
			return;
		}
		try {
			Backend::destroyDevice(device);
		} catch (std::exception& e) {
			Logging::beginError(8) << e.what();
			Logging::endError(8);
		}
		setupState = 0;
	}

}