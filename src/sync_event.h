#ifndef PROTO_SYNC_EVENT_H
#define PROTO_SYNC_EVENT_H

#include <linux/input.h>

#include "device_event.h"

namespace proto {

	class SyncEvent : public DeviceEvent
	{
	private:
			int report;
		public:
			SyncEvent(int report);
			~SyncEvent();
		public:
			int getType() override;
			int getDevice() override;
			int getReport();
	};

}

#endif // PROTO_SYNC_EVENT_H
