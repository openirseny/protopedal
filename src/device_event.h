#ifndef PROTO_DEVICE_EVENT_H
#define PROTO_DEVICE_EVENT_H

namespace proto
{
	class DeviceEvent {
	public:
		DeviceEvent() {};
		virtual ~DeviceEvent() {};
	public:
		virtual int getType() = 0;
		virtual int getDevice() = 0;
	};
}

#endif // PROTO_DEVICE_EVENT_H
