BUILD=build
SRC=src
OBJECTS=$(BUILD)/util.o $(BUILD)/logging.o $(BUILD)/axis_event.o $(BUILD)/button_event.o $(BUILD)/ffb_event.o $(BUILD)/idle_event.o \
	$(BUILD)/sync_event.o $(BUILD)/backend.o $(BUILD)/virtual_device.o $(BUILD)/physical_device.o $(BUILD)/device_setup.o $(BUILD)/device_mapper.o \
	$(BUILD)/curve_description.o $(BUILD)/event_plugin.o $(BUILD)/option_parser.o $(BUILD)/main.o

HEADERS=$(SRC)/util.h $(SRC)/logging.h $(SRC)/device_event.h $(SRC)/axis_event.h $(SRC)/button_event.h $(SRC)/ffb_event.h $(SRC)/sync_event.h $(SRC)/backend.h $(SRC)/virtual_device.h $(SRC)/physical_device.h $(SRC)/device_setup.h $(SRC)/device_mapper.h $(SRC)/option_parser.h

LDDFLAGS+= -Wl,--export-dynamic

.PHONY: all clean

all: build protopedal repeat.so

build: 
	mkdir -p $@

protopedal: $(OBJECTS)
	$(CXX) -g -o $@ $^ $(LDDFLAGS)
	
repeat.so: $(BUILD)/repeat_plugin.o
	$(CXX) -g -shared  -o $@ $^ $(LDDFLAGS)

$(BUILD)/%.o: $(SRC)/%.cpp $(HEADERS)
	$(CXX) -g -c $(CXXFLAGS) -fPIC -Wall -o $@ $<
	
clean:
	rm -f $(BUILD)/*.o protopedal repeat.so
